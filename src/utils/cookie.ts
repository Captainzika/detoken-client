const getCookie = (key: string): string => {
  const cookie = document.cookie
    .split(';')
    .filter((item) => item.trim().startsWith(`${key}=`));
  if (cookie.length) {
    return cookie[0].split('=')[1];
  }
  return '';
};

const setCookie = (key: string, value: string): void => {
  const domain = window.location.hostname.split('.').slice(1).join('.');
  let cookie = `${encodeURIComponent(key)}=${encodeURIComponent(value)}; `;
  cookie += 'path=/; ';
  cookie += `domain=${domain}; `;
  cookie += `max-age=${60 * 60 * 24 * 365}`; // 365 days
  document.cookie = cookie;
};

export { getCookie, setCookie };
