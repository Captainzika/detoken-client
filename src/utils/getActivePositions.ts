import { IFuturesPosition } from '~pages/PositionsPage/types';

interface IProps {
  positions: IFuturesPosition[];
  blockHeight: number;
}

export const getActivePositions = ({
  positions,
  blockHeight,
}: IProps): IFuturesPosition[] =>
  positions.map((position) => {
    const { maturityModifier, startBlockHeight } = position;
    return {
      ...position,
      isSettled: startBlockHeight + maturityModifier <= blockHeight,
    };
  });
