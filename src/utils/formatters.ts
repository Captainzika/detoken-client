import floor from 'lodash.floor';
import Big from 'big.js';
import {
  IListUnspentMessage,
  IUTXOs,
  IHistoryPayload,
  ITransactions,
  ITransaction,
  ITransactionRes,
} from '~dux/wallet/types';
import { CURRENCY_SYMBOL } from '~config/constants';

export const formatUTXOsResponse = (message: IListUnspentMessage): IUTXOs =>
  Object.entries(message).reduce(
    (acc, [address, value]) => ({
      ...acc,
      ...value.reduce((acc2, item) => {
        acc2[`${item.tx_hash}:${item.tx_pos}`] = {
          txid: item.tx_hash,
          outputIndex: item.tx_pos,
          address,
          amount: item.value,
          scriptPubKey: item.script,
        };
        return acc2;
      }, {} as IUTXOs),
    }),
    {}
  );

export const formatDeposits = ({
  history,
  activeAddress,
  contracts,
}: IHistoryPayload): ITransaction[] =>
  (history[activeAddress] || [])
    .filter((item) => !item.coinbase)
    .reduce((acc, { txid, time, vout, vin }) => {
      // check that tx is not contract related
      const isContractPayment =
        contracts.includes(vin[0].address) ||
        contracts.includes(vout[0].scriptPubKey.addresses[0]);
      const amount = vout.reduce(
        (acc2, { scriptPubKey, value }) =>
          scriptPubKey.addresses[0] === activeAddress && !isContractPayment
            ? Big(acc2).plus(value)
            : acc2,
        Big(0) as Big
      );
      if (Big(amount).gt(0)) {
        acc.push({
          txid,
          time: time || Math.round(new Date().getTime() / 1000),
          amount: amount.toString(),
          address: activeAddress,
        });
      }
      return acc;
    }, [] as ITransaction[]);

export const formatWithdrawals = ({
  history,
  activeAddress,
  changeAddress,
  contracts,
}: IHistoryPayload): ITransaction[] =>
  Object.values(history)
    .reduce((acc, txArray) => [...acc, ...txArray], [] as ITransactionRes[])
    .filter((item) => !item.coinbase)
    .reduce((acc, { txid, time, vin, vout }) => {
      const isWithdrawal = vin.find(
        ({ address }) => address === activeAddress || address === changeAddress
      );
      // check transaction was not created on contract opening
      const isContractPayment = contracts.includes(
        vout[0].scriptPubKey.addresses[0]
      );
      if (isWithdrawal && !isContractPayment) {
        const inputAmount = vin.reduce(
          (acc2, { address, value }) =>
            address === activeAddress || address === changeAddress
              ? Big(acc2).plus(value)
              : acc2,
          Big(0) as Big
        );

        const outputChangeAmount = vout.reduce(
          (acc2, { scriptPubKey, value }) =>
            scriptPubKey.addresses[0] === changeAddress
              ? Big(acc2).plus(value)
              : acc2,
          Big(0) as Big
        );

        const amount = Big(inputAmount).minus(outputChangeAmount);

        if (amount.gt(0) && !acc.find((item) => item.txid === txid)) {
          acc.push({
            txid,
            time: time || Math.round(new Date().getTime() / 1000),
            amount: amount.toString(),
            address: vout.find(
              (item) => item.scriptPubKey.addresses[0] !== changeAddress
            )?.scriptPubKey.addresses[0],
          });
        }
      }
      return acc;
    }, [] as ITransaction[]);

export const formatHistoryMessage = (
  props: IHistoryPayload
): ITransactions => ({
  received: formatDeposits(props),
  sent: formatWithdrawals(props),
});

const thousandsRegex = /(\d+)(\d{3})/;

export const separateThousands = (
  input: number | string,
  separator = ','
): string => {
  const str = input.toString();
  const parts = str.split('.');
  let result = parts[0];
  if (result.length <= 3) {
    return str;
  }

  while (thousandsRegex.test(result)) {
    result = result.replace(thousandsRegex, `$1${separator}$2`);
  }

  if (parts[1]) {
    return `${result}.${parts[1]}`;
  }
  if (str.slice(-1) === '.') {
    return `${result}.`;
  }
  return result;
};

export const formatFiat = (
  input: number | string,
  options: {
    removeFraction?: boolean;
    zeroFraction?: boolean;
    currency?: CURRENCY_SYMBOL;
  } = {}
): string => {
  const { removeFraction, zeroFraction, currency = '$' } = options;
  const number = floor(
    Math.abs(Number(input.toString().replace(/[^\d.]/g, ''))),
    2
  )
    .toFixed(2)
    .toString()
    .split('.');
  const whole = separateThousands(number[0]);
  const fraction = number[1];

  const fractionResult =
    !removeFraction && (Number(fraction) !== 0 || zeroFraction)
      ? `.${fraction}`
      : '';

  return `${currency}${whole}${fractionResult}`;
};

export type IDuration = [number, number];

export const blocksToDuration = (blocks: number): IDuration => {
  if (blocks <= 0) return [0, 0];

  let hours = Math.floor(blocks / 6);
  let days = 0;

  if (hours > 24) {
    days = Math.floor(blocks / 6 / 24);
    hours = Math.floor((blocks - days * 6 * 24) / 6);
  }
  return [days, hours];
};
