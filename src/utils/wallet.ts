import AES from 'crypto-js/aes';
import EncUtf8 from 'crypto-js/enc-utf8';
// @ts-expect-error
import HDPrivateKey, { fromSeed } from 'bitcore-lib-cash/lib/hdprivatekey';
import { mnemonicToSeedSync } from 'bip39';
import {
  getPublicKeyFromPrivateKey,
  getSinFromPublicKey,
  // @ts-expect-error
} from 'bitauth/lib/bitauth-browserify';
import Big from 'big.js';
// @ts-expect-error
import Transaction from 'bitcore-lib-cash/lib/transaction';

import { IUTXOs, IUTXO } from '~dux/wallet/types';
import { ISignedTransaction } from '~components/Modals/WithdrawModal/withdrawModalApi';
import { SATOSHIS_IN_BCH } from '~config/constants';

interface IGetPrivateKeyFromXpriv {
  xpriv: string;
  derivation?: string;
}

export const encrypt = (data: string, password: string): string => {
  const length = 32 - password.length; // we need password with 32 characters for 256 bits encryption
  const encKey = password + Array.from({ length }, () => 1).join('');
  return AES.encrypt(data, encKey).toString(); // encrypted mnemonic
};

interface IGetPrivateKeyFromMnemonic {
  mnemonic: string;
  derivation?: string;
}

export const getPrivateKeyFromMnemonic = ({
  mnemonic,
  derivation = "m/44'/145'/0'/0/0",
}: IGetPrivateKeyFromMnemonic): string => {
  const seedHex = mnemonicToSeedSync(mnemonic).toString('hex');
  const xpriv = fromSeed(seedHex).toString();
  const hdPrivateKey = new HDPrivateKey(xpriv);
  return hdPrivateKey.deriveChild(derivation).privateKey.toString();
};

interface IGetPublicKeyFromMnemonic {
  mnemonic: string;
  derivation?: string;
}

export const getPublicKeyFromMnemonic = ({
  mnemonic,
  derivation = "m/44'/145'/0'/0/0",
}: IGetPublicKeyFromMnemonic): string => {
  const privateKey = getPrivateKeyFromMnemonic({ mnemonic, derivation });
  return getPublicKeyFromPrivateKey(privateKey);
};

interface IGetSinFromMnemonic {
  mnemonic: string;
  derivation?: string;
}

export const getSinFromMnemonic = ({
  mnemonic,
  derivation = "m/44'/145'/0'/0/0",
}: IGetSinFromMnemonic): string => {
  const publicKey = getPublicKeyFromMnemonic({ mnemonic, derivation });
  return getSinFromPublicKey(publicKey);
};

export const decrypt = (data: string, password: string): string => {
  const length = 32 - password.length;
  const encKey = password + Array.from({ length }, () => 1).join('');
  return AES.decrypt(data, encKey).toString(EncUtf8);
};

export const getPrivateKeyFromXpriv = ({
  xpriv,
  derivation = "m/44'/145'/0'/0/0",
}: IGetPrivateKeyFromXpriv): string => {
  return new HDPrivateKey(xpriv).deriveChild(derivation).privateKey;
};

export const getActiveDerivation = (n = 0): string => `m/44'/145'/0'/0/${n}`;

export const getChangeDerivation = (n = 0): string => `m/44'/145'/0'/1/${n}`;

const OUTPUT_LOCKING_SCRIPT_LENGTH = 1; // (varInt) assumption
const OUTPUT_VALUE = 8;
const OUTPUT_SCRIPT_LENGTH = 25; // assumption

const CHANGE_OUTPUT_SIZE =
  OUTPUT_LOCKING_SCRIPT_LENGTH + OUTPUT_VALUE + OUTPUT_SCRIPT_LENGTH;

function calculateTransactionSize(tx: any): number {
  const TX_VERSION = 4;
  const TX_LOCKTIME = 4;

  const INPUT_COUNT = 1; // (varInt) assumption
  const OUTPUT_COUNT = 1; // (varInt) assumption

  const INPUT_UNLOCK_SCRIPT_LENGTH = 1; // (varInt) assumption

  const INPUT_PREV_TX_OUTPUT_INDEX = 4;
  const INPUT_PREV_TX_OUTPUT_HASH = 32;
  const INPUT_UNLOCK_SCRIPT = 107; // assumption
  const INPUT_SEQUENCE_NUMBER = 4;

  let total = TX_VERSION + TX_LOCKTIME + INPUT_COUNT + OUTPUT_COUNT;

  tx.inputs.forEach(() => {
    total += INPUT_PREV_TX_OUTPUT_INDEX;
    total += INPUT_PREV_TX_OUTPUT_HASH;
    total += INPUT_UNLOCK_SCRIPT_LENGTH;
    total += INPUT_SEQUENCE_NUMBER;
    total += INPUT_UNLOCK_SCRIPT;
  });

  tx.outputs.forEach((output: any) => {
    total += OUTPUT_LOCKING_SCRIPT_LENGTH;
    total += OUTPUT_VALUE;
    total += output.script.toBuffer().length;
  });

  return total;
}

interface ICreateSendTransaction {
  amount: Big;
  sendAddress: string;
  changeAddress: string;
  UTXOs: IUTXOs | null;
}

export const createSendTransaction = ({
  amount,
  sendAddress,
  changeAddress,
  UTXOs,
}: ICreateSendTransaction) => {
  let transaction = new Transaction();

  // work with satoshis only inside the transaction
  const satoshis = Number(amount.times(SATOSHIS_IN_BCH));
  const unspentOutputs: Array<IUTXO> = Object.values(UTXOs || {});

  transaction.to(sendAddress, satoshis);

  let txCost = satoshis + calculateTransactionSize(transaction);

  while (transaction.inputAmount < txCost && unspentOutputs.length) {
    transaction.from(unspentOutputs[unspentOutputs.length - 1]);
    unspentOutputs.pop();

    txCost = satoshis + calculateTransactionSize(transaction);
  }

  let deficit =
    transaction.outputAmount +
    calculateTransactionSize(transaction) -
    transaction.inputAmount;

  // while we don't have enough input satoshis to cover the network fee
  while (deficit > 0) {
    if (!unspentOutputs.length) {
      const max = amount.minus(Big(deficit).div(SATOSHIS_IN_BCH));
      transaction = createSendTransaction({
        amount: max,
        sendAddress,
        changeAddress,
        UTXOs,
      });
    } else {
      transaction.from(unspentOutputs[unspentOutputs.length - 1]);
      unspentOutputs.pop();

      txCost = satoshis + calculateTransactionSize(transaction);
    }

    deficit =
      transaction.outputAmount +
      calculateTransactionSize(transaction) -
      transaction.inputAmount;
  }

  const changeSatoshis = transaction.inputAmount - txCost - CHANGE_OUTPUT_SIZE;

  if (changeSatoshis >= 546) {
    transaction.to(changeAddress, changeSatoshis);
  }

  return transaction;
};

export const signSendTransaction = (
  transaction: any,
  keys: Array<string>
): ISignedTransaction => {
  // could throw
  transaction.sign(keys);

  if (!transaction.isFullySigned()) {
    throw Error('Transaction is not fully signed');
  }
  return transaction.toJSON();
};
