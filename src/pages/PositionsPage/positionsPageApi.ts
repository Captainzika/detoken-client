// @ts-expect-error
import { sign } from 'bitauth/lib/bitauth-browserify';
import { IFuturesPosition } from './types';
import request from '~providers/helpers';

interface IGetPositions {
  activePublicKey: string;
  activePrivateKey: string;
}
export const getFuturesPositions = ({
  activePrivateKey,
  activePublicKey,
}: IGetPositions): Promise<IFuturesPosition[]> => {
  const url = '/futures/positions';
  const timestamp = Date.now();
  const signature = sign(
    `${request.defaults.baseURL}${url}${timestamp}`,
    activePrivateKey
  ).toString('hex');

  return request.get(url, {
    headers: {
      'x-identity': activePublicKey,
      'x-signature': signature,
      'x-timestamp': timestamp,
    },
  });
};
