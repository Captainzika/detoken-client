import React, { FC, ReactElement, useCallback, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import styled, { Theme } from '~config/theme';
import { PageTitle, CheckBox, Scroll } from '~components';
import { IPositionsPage } from './types';
import { SendReceiveSection } from '~components/SendReceiveSection';
import { Position } from './components/Position';

export const PositionsPage: FC<IPositionsPage> = ({
  positions,
  blockHeight,
  openModal,
  oraclePrice,
  loading,
  settledContractTransactions,
  activeOnly,
  toggleActiveOnly,
}: IPositionsPage): ReactElement => {
  const { t } = useTranslation();

  const handleCheckboxClick = useCallback(() => {
    toggleActiveOnly();
  }, [toggleActiveOnly]);

  const handlePositionClick = useCallback(
    ({ metadata, parameters, simulation, contract, settlement }) => {
      openModal({
        type: 'position',
        data: {
          metadata,
          parameters,
          simulation,
          contract,
          oraclePrice,
          blockHeight,
          settlement,
        },
      });
    },
    [openModal, blockHeight, oraclePrice]
  );

  const visiblePositions = useMemo(
    () => (activeOnly ? positions.filter((p) => !p.isSettled) : positions),
    [positions, activeOnly]
  );

  return (
    <>
      <PageTitle title={`Detoken: ${t('positions_page')}`} />
      <SendReceiveSection />
      <Container>
        <HeaderContainer>
          <Header>{t('positions')}</Header>
          <Switcher>
            <CheckBox
              isChecked={activeOnly}
              handleClick={handleCheckboxClick}
            />
            <span>{t('only_show_active')}</span>
          </Switcher>
        </HeaderContainer>
        <Scroll autoHideBar>
          <TableWrapper>
            <TableContainer>
              <TableHeader first>{t('position')}</TableHeader>
              <TableHeader padding>{t('profit_loss')}</TableHeader>
              <TableHeader padding>{t('current_value')}</TableHeader>
              <TableHeader padding>{t('start_value')}</TableHeader>
              <TableHeader padding>{t('start_price')}</TableHeader>

              <TableHeader padding alignRight>
                {t('duration')}
              </TableHeader>
              <TableHeader padding alignRight>
                {t('expires_in')}
              </TableHeader>
              <TableHeader padding last>
                &nbsp;
              </TableHeader>
              {!loading &&
                !!visiblePositions.length &&
                visiblePositions.map((position, index) => (
                  <Position
                    key={position.address}
                    index={index}
                    data={position}
                    onClick={handlePositionClick}
                    blockHeight={blockHeight}
                    oraclePrice={oraclePrice}
                    settlementTransaction={
                      settledContractTransactions[position.address]
                    }
                  />
                ))}
            </TableContainer>
          </TableWrapper>
        </Scroll>
        {loading && <InfoWrapper>{t('loading')}</InfoWrapper>}
        {!loading && !visiblePositions.length && (
          <InfoWrapper>{t('no_positions')}</InfoWrapper>
        )}
      </Container>
    </>
  );
};

const Container = styled.section`
  display: flex;
  flex-direction: column;
  margin-top: 12px;
  box-sizing: border-box;
  text-align: center;
  line-height: 1.6;
  height: calc(100vh - 100px);
  max-width: 100vw;

  ${({ theme }): string => `
    ${theme.mediaTablet} {
      height: calc(100vh - 143px);
      padding: 0;
      margin: 20px 15px;
      background: ${theme.background.primary};
      border: 1px solid ${theme.border.default};
      border-radius: 4px;
      max-width: calc(100vw - 30px);
    }
    ${theme.mediaDesktop}{
      margin: 20px 60px;
      max-width: calc(100vw - 120px);
    }`};
`;

const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 20px;
  height: 54px;
  min-height: 54px;
`;

const Header = styled.div`
  text-transform: uppercase;
  color: ${({ theme }): string => theme.text.header};
  font-size: 1rem;
  line-height: 21px;
  font-weight: bold;
`;

const Switcher = styled.div`
  display: flex;
  color: ${({ theme }): string => theme.text.primary};
  & > span {
    white-space: nowrap;
    margin-left: 23px;
  }

  & > div {
    align-self: center;
  }
`;

const TableWrapper = styled.div`
  height: 100%;
  width: 100%;
  max-width: 100vw;
  overflow: auto;
  position: relative;
  ${({ theme }): string => `
    ${theme.mediaTablet}{
      max-width: calc(100vw - 30px);
    }
  `}

  ::-webkit-scrollbar {
    height: 3px;
    background: ${({ theme }): string => theme.background.secondary};
  }
  ::-webkit-scrollbar-thumb:horizontal {
    background: ${({ theme }): string => theme.border.section};
    border-radius: 2px;
  }
  ${({ theme }): string => `
    ${theme.mediaTablet}{
      ::-webkit-scrollbar {
        background: ${theme.background.primary};
      }
      ::-webkit-scrollbar-thumb:horizontal {
        background: ${theme.border.default};
      }
    }
  `}
`;

const TableContainer = styled.div`
  display: grid;
  grid-template-columns:
    100px minmax(165px, max-content) max-content minmax(219px, max-content)
    auto 100px max-content 65px;
  grid-template-rows: auto;
  text-align: left;
  & > div {
    white-space: nowrap;
  }
  ${({ theme }): string => `
    ${theme.mediaDesktop}{
      grid-template-columns: 100px minmax(183px, max-content) max-content minmax(
      243px,
      max-content
    ) auto 100px max-content 65px;
    }
  `};
`;

interface ITableHeader {
  padding?: boolean;
  first?: boolean;
  last?: boolean;
  alignRight?: boolean;
  theme: Theme;
}

const TableHeader = styled.div`
  border-top: 1px solid ${({ theme }): string => theme.border.section};
  border-bottom: 1px solid ${({ theme }): string => theme.border.section};
  height: fit-content;
  padding-left: ${({ padding }: ITableHeader): string =>
    padding ? '25px' : ''};
  ${({ first }: ITableHeader): string => (first ? 'padding-left: 20px' : '')};
  ${({ last }: ITableHeader): string => (last ? 'padding-right: 20px;' : '')};
  color: ${({ theme }): string => theme.text.fiat};
  margin-bottom: 20px;
  ${({ theme }): string => `
    ${theme.mediaTablet}{
      border-top: 1px solid ${theme.border.default};
      border-bottom: 1px solid ${theme.border.default};
    }
  `};
  text-align: ${({ alignRight }): string => (alignRight ? 'right' : 'left')};
`;

const InfoWrapper = styled.div`
  height: 100%;
  color: ${({ theme }): string => theme.text.secondary};
`;
