import { IState } from '~dux/rootDux';
import { OpenModalAction } from '~dux/modals/modalsDux';
import { POSITION } from '~config/constants';

export interface IMapDispatch {
  openModal: (props: OpenModalAction) => void;
  toggleActiveOnly: () => void;
}

export interface IMapState {
  positions: IState['positionsPage']['positions'];
  loading: IState['positionsPage']['loading'];
  blockHeight: IState['app']['oraclePrice']['blockHeight'];
  oraclePrice: IState['app']['oraclePrice']['price'];
  settledContractTransactions: { [address: string]: string };
  activeOnly: IState['positionsPage']['activeOnly'];
}

export interface IPositionsPage extends IMapState, IMapDispatch {}

export interface IPosition {
  id: string;
  address: string;
  oraclePubKey: string;
  takerSide: POSITION;
  hedgeUnits: number;
  startPrice: number;
  lowLiquidationMulti: string;
  highLiquidationMulti: string;
  createdAt: string;
  startBlockHeight: number;
  maturityModifier: number;
  premium: number;
  fee: string;
}

export interface IFuturesPosition extends IPosition {
  isSettled: boolean;
}
