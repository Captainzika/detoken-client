import React, { FC, ReactElement, useEffect } from 'react';
import { Redirect } from 'react-router-dom';

interface IProps {
  isAuthorized: boolean;
  showNotification: () => void;
}
export const NotFoundPage: FC<IProps> = ({
  isAuthorized,
  showNotification,
}: IProps): ReactElement => {
  useEffect(() => showNotification());
  return isAuthorized ? (
    <Redirect to="/dashboard" />
  ) : (
    <Redirect to="/signin" />
  );
};
