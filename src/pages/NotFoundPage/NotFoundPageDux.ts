import { Dispatch } from 'redux';
import { errorMessage } from '~dux/app/appDux';
import { ERROR_KEYS } from '~types/Error';

type Action = ReturnType<typeof errorMessage>;

export const NotFoundNotification = () => (
  // @ts-expect-error
  dispatch: Dispatch<Action>
): void => {
  dispatch(errorMessage({ key: ERROR_KEYS.PAGE_NOT_FOUND }));
};
