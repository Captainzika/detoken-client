import { connect } from 'react-redux';
import { IState } from '~dux/rootDux';
import { ResetPasswordPage } from './ResetPasswordPage';
import {
  resetPassword,
  resetPasswordPageErrorsSelector,
  setError,
} from './ResetPasswordPageDux';
import { IMapDispatch, IMapState } from './types';

const mapDispatchToProps: IMapDispatch = {
  resetPassword,
  setError,
};

const mapStateToProps = (state: IState): IMapState => ({
  ...state.resetPasswordPage,
  error: resetPasswordPageErrorsSelector(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(ResetPasswordPage);
