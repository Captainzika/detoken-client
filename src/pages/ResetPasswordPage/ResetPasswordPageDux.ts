import { validateMnemonic } from 'bip39';
import { Dispatch } from 'redux';
import { batch } from 'react-redux';
import { createAction, createReducer } from 'redux-act';
import { createSelector } from 'reselect';
import i18next from 'i18next';
import { IState } from '~dux/rootDux';
import { IError, ERROR_KEYS } from '~types/Error';
import { encrypt, getSinFromMnemonic } from '~utils/wallet';
import { getHash } from '~utils/password';
import { addSnackbarAction } from '~dux/notifications/notificationsDux';
import { resetPassword as passwordResetRequest } from './ResetPasswordPageApi';

export interface IResetPasswordPageState {
  isCompleted: boolean;
  loading: boolean;
  error: IError | null;
}

const defaultState: IResetPasswordPageState = {
  isCompleted: false,
  loading: false,
  error: null,
};

const duxSelector = (state: IState): IResetPasswordPageState =>
  state.resetPasswordPage;

export const resetPasswordPageErrorsSelector = createSelector(
  duxSelector,
  ({ error }) => error
);

const setErrorAction = createAction<IError | null>('setErrorAction');
const setLoadingAction = createAction('setLoadingAction');
const isCompletedAction = createAction('isCompletedAction');
const checkPasswordAction = createAction<string>('checkPasswordAction');

type Action = ReturnType<
  | typeof setErrorAction
  | typeof setLoadingAction
  | typeof isCompletedAction
  | typeof checkPasswordAction
  | typeof addSnackbarAction
>;

export interface IResetPassword {
  words: string;
  password: string;
  code: string;
  email: string;
}

export const setError = (err: IError | null) => (
  dispatch: Dispatch<Action>
): void => {
  dispatch(setErrorAction(err));
};

export const resetPassword = ({
  words,
  password,
  code,
  email,
}: IResetPassword) => async (dispatch: Dispatch<Action>): Promise<void> => {
  dispatch(setLoadingAction());
  const phrase = words.replace(/\s+/g, ' ').trim();
  try {
    if (!validateMnemonic(phrase)) {
      dispatch(
        setErrorAction({
          key: ERROR_KEYS.WRONG_MNEMONIC_PHRASE,
          field: 'words',
        })
      );
      return;
    }
  } catch (e) {
    dispatch(
      setErrorAction({
        key: ERROR_KEYS.WRONG_MNEMONIC_PHRASE,
        field: 'words',
      })
    );
    return;
  }
  try {
    const encryptedMnemonic = encrypt(phrase, password);
    const sin = getSinFromMnemonic({ mnemonic: phrase });

    await passwordResetRequest({
      encryptedMnemonic,
      passwordHash: getHash(password),
      email,
      code,
      sin,
    });
    batch(() => {
      dispatch(isCompletedAction());
      dispatch(
        addSnackbarAction({ message: i18next.t('success_reset_password') })
      );
    });
  } catch (err) {
    const { field, key } = err || {};
    if (field === 'code' || field === 'email') {
      batch(() => {
        dispatch(isCompletedAction());
        dispatch(
          addSnackbarAction({ message: i18next.t(key), variant: 'error' })
        );
      });
    } else {
      dispatch(setErrorAction(err));
    }
  }
};

export default createReducer<IResetPasswordPageState>(
  {
    [setLoadingAction.toString()]: (state: IResetPasswordPageState) => ({
      ...state,
      loading: true,
    }),

    [setErrorAction.toString()]: (
      state: IResetPasswordPageState,
      error: IError | null
    ) => ({
      ...state,
      error,
      loading: false,
    }),

    [isCompletedAction.toString()]: (state: IResetPasswordPageState) => ({
      ...state,
      loading: false,
      isCompleted: true,
    }),
  },
  defaultState
);
