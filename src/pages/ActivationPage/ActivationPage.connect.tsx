import { connect } from 'react-redux';
import { ActivationPage } from './ActivationPage';
import { IState } from '~dux/rootDux';
import {
  activationPageSuccessSelector,
  activationPageErrorsSelector,
  requestActivationLink,
} from './activationPageDux';
import { IError } from '~types/Error';

interface IConnectProps {
  success: boolean;
  error: IError | null;
}

const mapDispatchToProps = {
  requestActivationLink,
};

const mapStateToProps = (state: IState): IConnectProps => ({
  success: activationPageSuccessSelector(state),
  error: activationPageErrorsSelector(state) || null,
});

export default connect(mapStateToProps, mapDispatchToProps)(ActivationPage);
