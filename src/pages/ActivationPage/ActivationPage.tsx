import React, { FC, useEffect } from 'react';
import { Redirect, RouteComponentProps } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useQuery } from '~hooks/useQuery';
import styled from '~config/theme';
import { IError } from '~types/Error';
import { FormLayout, PageTitle } from '~components';
import { FullScreenLoader } from '~components/FullScreenLoader';
import FailSvg from '~icons/fail.svg';

interface IProps extends RouteComponentProps {
  isLoading: boolean;
  success: boolean;
  error: IError | null;
  requestActivationLink: ({
    email,
    code,
  }: {
    email: string;
    code: string;
  }) => void;
}

const ActivationPage: FC<IProps> = ({
  success,
  error,
  requestActivationLink,
}: IProps): React.ReactElement => {
  const code = useQuery().get('code') || '';
  const email = useQuery().get('email') || '';
  const { key, field } = error || {};
  const { t } = useTranslation();

  const isError = key || field;

  useEffect(() => {
    requestActivationLink({ email, code });
  }, [email, code, requestActivationLink]);

  return (
    <Container>
      <PageTitle title={`Detoken: ${t('email_activation_page')}`} />
      {/* We can manipulate with main error if it's need */}
      {success && <Redirect to="/signin" />}
      {isError && (
        <FormLayout logo icon={<StyledFail />}>
          {key && !field && <Error>{t(key)}</Error>}
          {field === 'email' && key && (
            <Error>{t(key, { field: t('email') })}</Error>
          )}
          {field === 'code' && key && (
            <Error>{t(key, { field: t('code') })}</Error>
          )}
        </FormLayout>
      )}
      {!isError && <FullScreenLoader />}
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  height: 100%;
`;

const Error = styled.div`
  font-size: calc(4rem / 3);
  font-weight: bold;
  text-align: center;
`;

const StyledFail = styled(FailSvg as 'img')`
  fill: ${({ theme }): string => theme.icons.fail};
`;

export { ActivationPage };
