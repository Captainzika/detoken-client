import { createAction, createReducer } from 'redux-act';
import { createSelector } from 'reselect';
import { Dispatch } from 'redux';
import { batch } from 'react-redux';
import i18next from 'i18next';
import { sentConfirmationEmail } from './activationPageApi';
import { IError } from '~types/Error';
import { IState } from '~dux/rootDux';
import { addSnackbarAction } from '~dux/notifications/notificationsDux';

export interface IActivationPageState {
  success: boolean;
  error: IError | null;
}

interface IActivationLinkProps {
  code: string;
  email: string;
}

const DEFAULT_STATE = {
  error: null,
  success: false,
};

// Selectors
const duxSelector = (state: IState): IActivationPageState =>
  state.activationPage;

export const activationPageSuccessSelector = createSelector(
  duxSelector,
  ({ success }) => success
);

export const activationPageErrorsSelector = createSelector(
  duxSelector,
  ({ error }) => error
);

// Sync Actions
const success = createAction('success');
const failed = createAction<IError>('failed');

// Async Actions
export const requestActivationLink = ({
  code,
  email,
}: IActivationLinkProps) => async (dispatch: Dispatch): Promise<void> => {
  try {
    await sentConfirmationEmail({ code, email });
    batch(() => {
      dispatch(success());
      dispatch(
        addSnackbarAction({
          key: 'email_confirmed',
          message: i18next.t('email_confirmed'),
        })
      );
    });
  } catch (e) {
    dispatch(failed(e));
  }
};

export default createReducer<IActivationPageState>(
  {
    [success.toString()]: (state: IActivationPageState) => ({
      ...state,
      success: true,
    }),
    [failed.toString()]: (state: IActivationPageState, error: IError) => ({
      ...state,
      error,
    }),
  },
  DEFAULT_STATE
);
