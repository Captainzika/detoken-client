import { IDisable2FA } from '~components/Modals/Disable2FAModal/Disable2FAModalDux';
import { twoFAModeType, IUserState } from '~dux/user/userDux';
import { IAccountPageState, IChangePassword } from './accountPageDux';
import { OpenModalAction } from '~dux/modals/modalsDux';

export interface IMapDispatch {
  disable2FA: ({ password, code }: IDisable2FA) => void;
  changePassword: (props: IChangePassword) => void;
  resetError: () => void;
  openModal: (props: OpenModalAction) => void;
}

export interface IMapState extends IAccountPageState {
  twoFAMode: twoFAModeType;
  userName: IUserState['name'];
  refCode: IUserState['refCode'];
  isLoading: IAccountPageState['isLoading'];
  error: IAccountPageState['error'];
}

export interface IAccountPageProps extends IMapState, IMapDispatch {}
