import React, {
  FC,
  ReactElement,
  useCallback,
  useState,
  useEffect,
} from 'react';
import { useTranslation } from 'react-i18next';
import { TextField, Form } from '~components';
import { IAccountPageProps } from '~pages/AccountPage/types';

interface IProps {
  success: IAccountPageProps['success'];
  error: IAccountPageProps['error'];
  isLoading: IAccountPageProps['isLoading'];
  changePassword: IAccountPageProps['changePassword'];
  resetError: IAccountPageProps['resetError'];
}

export const AccountPageChangePasswordForm: FC<IProps> = ({
  success,
  error,
  isLoading,
  changePassword,
  resetError,
}: IProps): ReactElement => {
  const { key, field, params } = error || {};
  const { t } = useTranslation();
  const [currentPassword, setCurrentPassword] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const handlers: {
    [key: string]: (prop: string) => void;
  } = {
    currentPassword: setCurrentPassword,
    password: setPassword,
    confirmPassword: setConfirmPassword,
  };

  useEffect(() => {
    if (success) {
      setCurrentPassword('');
      setPassword('');
      setConfirmPassword('');
    }
  }, [success]);

  const handleFormSubmit = useCallback(
    (e: React.FormEvent) => {
      e.preventDefault();
      changePassword({
        currentPassword,
        password,
        confirmPassword,
      });
    },
    [changePassword, currentPassword, password, confirmPassword]
  );

  const handleInputChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      resetError();
      const { id, value } = e.currentTarget;
      handlers[id](value);
    },
    [resetError, handlers]
  );

  return (
    <Form
      onSubmit={handleFormSubmit}
      submitText={t('change_password_button')}
      submitDisabled={isLoading}
    >
      <TextField
        id="currentPassword"
        name="currentPassword"
        type="password"
        onChange={handleInputChange}
        value={currentPassword}
        placeholder={t('current_password_placeholder')}
        error={field === 'passwordHash'}
        helperText={key && field === 'passwordHash' ? t(key, params) : ''}
      />
      <TextField
        id="password"
        name="password"
        type="password"
        onChange={handleInputChange}
        value={password}
        placeholder={t('new_password_placeholder')}
        error={field === 'newPasswordHash'}
        helperText={key && field === 'newPasswordHash' ? t(key, params) : ''}
      />
      <TextField
        id="confirmPassword"
        name="confirmPassword"
        type="password"
        onChange={handleInputChange}
        value={confirmPassword}
        placeholder={t('repeat_new_password_placeholder')}
        error={field === 'confirmPassword'}
        helperText={key && field === 'confirmPassword' ? t(key, params) : ''}
      />
    </Form>
  );
};
