import { Dispatch } from 'redux';
import { batch } from 'react-redux';
import { createAction, createReducer } from 'redux-act';
import i18next from 'i18next';
import { ERROR_KEYS, IError } from '~types/Error';
import { IState } from '~dux/rootDux';
import { validatePassword } from '~utils/validation';
import { decrypt, encrypt } from '~utils/wallet';
import { getHash } from '~utils/password';
import { changePassword as changePasswordRequest } from './accountPageApi';
import { addSnackbarAction } from '~dux/notifications/notificationsDux';
import { updateEncryptedMnemonic } from '~dux/user/userDux';

export interface IAccountPageState {
  isLoading: boolean;
  success: boolean;
  error: IError | null;
}

export interface IChangePassword {
  currentPassword: string;
  password: string;
  confirmPassword: string;
}

const DEFAULT_STATE = {
  isLoading: false,
  error: null,
  success: false,
};

// Sync Actions
const loading = createAction('loading');
const success = createAction('success');
const setError = createAction<IError | null>('setError');

type Action = ReturnType<
  | typeof loading
  | typeof success
  | typeof setError
  | typeof addSnackbarAction
  | typeof updateEncryptedMnemonic
>;

export const resetError = () => (dispatch: Dispatch<Action>): void => {
  dispatch(setError(null));
};

// Async actions
export const changePassword = ({
  currentPassword,
  password,
  confirmPassword,
}: IChangePassword) => async (
  dispatch: Dispatch<Action>,
  getState: () => IState
): Promise<void> => {
  dispatch(loading());
  if (!currentPassword) {
    const t = i18next.t('current_password');
    dispatch(
      setError({
        key: ERROR_KEYS.REQUIRED,
        field: 'passwordHash',
        params: { field: t },
      })
    );
    return;
  }
  if (!password) {
    const t = i18next.t('new_password');
    dispatch(
      setError({
        key: ERROR_KEYS.REQUIRED,
        field: 'newPasswordHash',
        params: { field: t },
      })
    );
    return;
  }
  if (!confirmPassword) {
    const t = i18next.t('new_password');
    dispatch(
      setError({
        key: ERROR_KEYS.REQUIRED,
        field: 'confirmPassword',
        params: { field: t },
      })
    );
    return;
  }

  const oldPasError = validatePassword(currentPassword);
  const newPasError = validatePassword(password);
  const error = oldPasError || newPasError;
  if (error) {
    dispatch(
      setError({
        ...error,
        field: oldPasError ? 'passwordHash' : 'newPasswordHash',
      })
    );
    return;
  }
  if (password !== confirmPassword) {
    dispatch(
      setError({
        key: ERROR_KEYS.PASSWORDS_MISMATCH,
        field: 'confirmPassword',
      })
    );
    return;
  }
  const userData = getState().user;
  const { encryptedMnemonic, sin, email } = userData;
  const mnemonic = decrypt(encryptedMnemonic, currentPassword);
  const newEncryptedMnemonic = encrypt(mnemonic, password);
  const newPasswordHash = getHash(password);
  const passwordHash = getHash(currentPassword);

  try {
    await changePasswordRequest({
      sin,
      newEncryptedMnemonic,
      newPasswordHash,
      passwordHash,
      email,
    });
    batch(() => {
      dispatch(success());
      dispatch(
        addSnackbarAction({
          variant: 'success',
          message: i18next.t('password_changed'),
        })
      );
      dispatch(updateEncryptedMnemonic(newEncryptedMnemonic));
    });
  } catch (err) {
    dispatch(setError(err));
  }
};

export default createReducer<IAccountPageState>(
  {
    [loading.toString()]: (state: IAccountPageState) => ({
      ...state,
      success: false,
      isLoading: true,
    }),

    [success.toString()]: (state: IAccountPageState) => ({
      ...state,
      success: true,
      isLoading: false,
    }),

    [setError.toString()]: (
      state: IAccountPageState,
      error: IError | null
    ) => ({
      ...state,
      error,
      isLoading: false,
    }),
  },
  DEFAULT_STATE
);
