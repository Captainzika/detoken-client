import request from '~providers//helpers';

const { patch } = request;

export interface IChangePassword {
  newPasswordHash: string;
  passwordHash: string;
  newEncryptedMnemonic: string;
  sin: string;
  email: string;
}

export const changePassword = async (body: IChangePassword): Promise<void> =>
  patch('/password', body);
