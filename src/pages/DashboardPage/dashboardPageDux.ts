import { createAction, createReducer } from 'redux-act';
import { Dispatch } from 'react';
import Big from 'big.js';
import { createSelector } from 'reselect';
import { batch } from 'react-redux';
import i18next from 'i18next';
import { AnyHedgeManager } from '@generalprotocols/anyhedge';
import { ERROR_KEYS, IError } from '~types/Error';
import { IState } from '~dux/rootDux';
import { oraclePriceSelector, sendWSMessageAction } from '~dux/app/appDux';
import {
  receiveUTXOsMessage,
  receiveHistoryMessage,
  UTXOsSelector,
  walletBalanceSelector,
  removeUTXO,
} from '~dux/wallet/walletDux';
import {
  addSnackbarAction,
  removeSnackbarAction,
} from '~dux/notifications/notificationsDux';
import {
  POSITION,
  CONTRACTS,
  SATOSHIS_IN_BCH,
  MAX_HEDGE_UNITS,
  MIN_HEDGE_UNITS,
  BCH_DP,
  DUST_SATOSHIS,
} from '~config/constants';
import {
  getEntryData,
  IEntryDataRes,
  IContractData,
  postContract,
  IPostContract,
} from './dashboardPageApi';
import {
  getPublicKeysSelector,
  getPrivateKeysSelector,
  getAddressesSelector,
} from '~dux/user/userDux';
import { openModal, closeModal } from '~dux/modals/modalsDux';
import { IFuture } from '~dux/app/appApi';
import { getMargin } from '~utils/contract';
import {
  createPartialFundingTransaction,
  signPartialFundingTransaction,
} from '~utils/funding';
import { addPositionAction } from '~pages/PositionsPage/positionsPageDux';

interface IPremiums {
  hedgePremium: number;
  longPremium: number;
}

interface IPrices {
  hedgePrice: number;
  longPrice: number;
}

interface IMaximums {
  maxHedgeSatoshis: number;
  maxLongSatoshis: number;
}

interface IPremiumsPrices {
  premiums: IPremiums;
  prices: IPrices;
  maximums: IMaximums;
}

export interface IDashboardPageState {
  isLoading: boolean;
  error: IError | null;
  success: boolean;
  premiums: Partial<IPremiums>;
  prices: Partial<IPrices>;
  maximums: Partial<IMaximums>;
  contractData: IContractData | {};
  amount: string;
  currency: string;
}

export interface IContractInfoProps {
  position: POSITION;
  amount: string;
  productId: typeof CONTRACTS[number];
}

const DEFAULT_STATE = {
  isLoading: false,
  error: null,
  success: false,
  premiums: {},
  prices: {},
  maximums: {},
  contractData: {},
  amount: '',
  currency: 'BCH',
};

// Selectors
const duxSelector = (state: IState): IDashboardPageState => state.dashboardPage;
const manager = new AnyHedgeManager();

export const isDashboardPageLoadingSelector = createSelector(
  (state: IState) => state,
  ({ app, dashboardPage }) =>
    app.isDataServerConnecting || dashboardPage.isLoading
);

export const dashboardPageErrorsSelector = createSelector(
  duxSelector,
  ({ error }) => error
);

// Sync Actions
const loading = createAction('loading');
const success = createAction('success');
const failed = createAction<IError | null>('failed');

const updatePremiumsPricesAction = createAction<IPremiumsPrices>(
  'updatePremiumsPricesAction'
);
const saveContractDataAction = createAction<IContractData>(
  'saveContractDataAction'
);
const setAmountAction = createAction<string>('setAmountAction');

const setCurrencyAction = createAction<string>('setCurrencyAction');

export const setError = createAction<IError | null>('setError');

type Action = ReturnType<
  | typeof receiveUTXOsMessage
  | typeof receiveHistoryMessage
  | typeof failed
  | typeof loading
  | typeof addSnackbarAction
  | typeof openModal
  | typeof updatePremiumsPricesAction
  | typeof saveContractDataAction
  | typeof removeSnackbarAction
  | typeof closeModal
  | typeof setAmountAction
  | typeof addPositionAction
>;

export const getWalletData = (props: {
  activeAddress: string;
  changeAddress: string;
}) => async (dispatch: Dispatch<Action>): Promise<void> => {
  const { activeAddress, changeAddress } = props;
  const addresses = [activeAddress, changeAddress];
  dispatch(sendWSMessageAction({ type: 'list_unspent', addresses }));
};

// TODO: this function is used to provide fresh data for calculating user earning in block below the start contract one
// it should be removed once we'll move entry data to WS
export const getPremiumPriceData = (
  productId: typeof CONTRACTS[number]
) => async (
  dispatch: Dispatch<Action>,
  getState: () => IState
): Promise<void> => {
  const state = getState();
  const { activePrivateKey } = getPrivateKeysSelector(state);
  const { activePublicKey } = getPublicKeysSelector(state);
  try {
    const {
      hedgePremium,
      longPremium,
      hedgePrice,
      longPrice,
      maxHedgeSatoshis,
      maxLongSatoshis,
    } = await getEntryData({
      productId,
      activePublicKey,
      activePrivateKey,
    });
    dispatch(
      updatePremiumsPricesAction({
        premiums: { hedgePremium, longPremium },
        prices: { hedgePrice, longPrice },
        maximums: { maxHedgeSatoshis, maxLongSatoshis },
      })
    );
    // eslint-disable-next-line no-empty
  } catch (e) {}
};

export const updateEntryData = (
  dispatch: Dispatch<Action>,
  getState: () => IState
): void => {
  CONTRACTS.forEach((contract) =>
    getPremiumPriceData(contract)(dispatch, getState)
  );
};

export const setAmount = (amount: string) => (
  dispatch: Dispatch<Action>
): void => {
  dispatch(setAmountAction(amount));
};

export const setCurrency = (currency: string) => (
  dispatch: Dispatch<Action>
): void => {
  dispatch(setCurrencyAction(currency));
};

export const sendContract = () => async (
  dispatch: Dispatch<Action>,
  getState: () => IState
): Promise<void> => {
  batch(() => {
    dispatch(removeSnackbarAction(''));
    dispatch(loading());
  });
  const state = getState();
  const { activePrivateKey } = getPrivateKeysSelector(state);
  const { activePublicKey } = getPublicKeysSelector(state);
  const { contractData } = state.dashboardPage;
  const { id, usedUTXOs } = contractData as IContractData;
  try {
    const response = await postContract({
      data: contractData,
      activePrivateKey,
      activePublicKey,
    } as IPostContract);
    batch(() => {
      const { takerSide } = response;
      dispatch(
        addSnackbarAction({
          message: i18next.t('positions_created', { side: takerSide }),
          variant: 'success',
        })
      );
      dispatch(closeModal({ type: 'longHedgeConfirm' }));
      dispatch(setAmountAction(''));
      dispatch(
        addPositionAction(Object.assign(response, { isSettled: false }))
      );
      usedUTXOs.forEach((utxo) => dispatch(removeUTXO(utxo)));
      dispatch(success());
    });
    await getPremiumPriceData(id)(dispatch, getState);
  } catch (err) {
    const { field, key } = err;
    batch(() => {
      dispatch(
        addSnackbarAction({
          message: i18next.t(key, { field: i18next.t(field) }),
          variant: 'error',
        })
      );
      dispatch(success());
    });
  }
};

export const showContractInfo = ({
  amount, // in BCH
  position,
  productId,
}: IContractInfoProps) => async (
  dispatch: Dispatch<Action>,
  getState: () => IState
): Promise<void> => {
  dispatch(loading());

  const state = getState();
  const isModalOpen = state.modals.longHedgeConfirm.isOpen;
  const { HEDGE } = POSITION;
  const { futuresList } = state.app; // config from server
  const {
    activePrivateKey,
    rawActivePrivateKey,
    rawChangePrivateKey,
  } = getPrivateKeysSelector(state);
  const { activePublicKey } = getPublicKeysSelector(state);
  const { activeAddress, changeAddress } = getAddressesSelector(state);
  const balance = walletBalanceSelector(state);
  const { currency } = state.dashboardPage;
  let entryData = {} as IEntryDataRes;
  let contract;

  // Get product config by id
  const {
    oraclePubKey,
    maturityModifier,
    fee,
    lowLiquidationMulti,
    highLiquidationMulti,
    minHedgeSatoshis,
    minLongSatoshis,
  } = futuresList.find(({ id }) => id === productId) as IFuture;

  // Get entry data
  try {
    entryData = await getEntryData({
      productId,
      activePublicKey,
      activePrivateKey,
    });
  } catch (e) {
    batch(() => {
      dispatch(failed(e));
      dispatch(
        addSnackbarAction({ message: 'e.something_wrong', variant: 'error' })
      );
    });
    return;
  }

  const {
    feeAddress,
    hedgePremium,
    longPremium,
    startBlockHeight,
    hedgePrice,
    longPrice,
    maxLongSatoshis,
    maxHedgeSatoshis,
    makerPubKey,
  } = entryData;

  // Prepare contract data
  const startPrice = position === HEDGE ? hedgePrice : longPrice;
  const margin = getMargin(lowLiquidationMulti);
  // we need us cents here
  let hedgeUnits;
  if (currency === 'USD') {
    hedgeUnits =
      position === HEDGE
        ? Big(amount).times(100).toFixed()
        : Big(amount).times(100).times(margin).toFixed();
  } else {
    hedgeUnits =
      position === HEDGE
        ? Big(amount).times(hedgePrice).round(0, 0).toString()
        : Big(amount).times(longPrice).round(0, 0).times(margin).toString();
  }

  if (Big(hedgeUnits).lt(MIN_HEDGE_UNITS)) {
    dispatch(
      failed({
        key: ERROR_KEYS.CONTRACT_AMOUNT_TOO_SMALL,
        field: 'amount',
      })
    );
    return;
  }

  // calculate max and min for the contract
  const maxAhCents =
    position === HEDGE
      ? Big(MAX_HEDGE_UNITS)
      : Big(MAX_HEDGE_UNITS).div(margin).round(0, 0);
  const maxAhSatoshis = maxAhCents
    .div(startPrice)
    .round(8, 0)
    .mul(SATOSHIS_IN_BCH);
  const minAhCents =
    position === HEDGE
      ? Big(MIN_HEDGE_UNITS)
      : Big(MIN_HEDGE_UNITS).div(margin).round(0, 3);
  const minAhSatoshis = minAhCents
    .div(startPrice)
    .round(8, 3)
    .mul(SATOSHIS_IN_BCH);

  const maxMMTakerSatoshis =
    position === HEDGE
      ? Big(maxLongSatoshis).times(margin).round(0, 3)
      : Big(maxHedgeSatoshis).div(margin).round(0, 3);
  const maxTakerSatoshis = Math.min(
    Number(maxAhSatoshis),
    Number(maxMMTakerSatoshis)
  );

  const minMMTakerSatoshis =
    position === HEDGE ? minHedgeSatoshis : minLongSatoshis;
  const minTakerSatoshis = Math.max(Number(minAhSatoshis), minMMTakerSatoshis);

  const hedgePublicKey = position === HEDGE ? activePublicKey : makerPubKey;
  const longPublicKey = position === HEDGE ? makerPubKey : activePublicKey;

  // Create a contract
  try {
    contract = await manager.createContract(
      oraclePubKey,
      hedgePublicKey,
      longPublicKey,
      Number(hedgeUnits), // expected number in BCH
      startPrice, // expected number
      startBlockHeight,
      0,
      maturityModifier,
      Number(highLiquidationMulti), // expected number
      Number(lowLiquidationMulti) // expected number
    );
  } catch (e) {
    dispatch(
      failed({ field: 'amount', key: ERROR_KEYS.INVALID_CONTRACT_INPUTS })
    );
    return;
  }

  const { address: contractAddress, metadata, parameters } = contract;
  const { longInputSats, hedgeInputSats, minerCost, dustCost } = metadata;

  const takerInputSatoshis =
    position === HEDGE ? hedgeInputSats : longInputSats;
  const makerInputSatoshis =
    position === HEDGE ? longInputSats : hedgeInputSats;

  // Dispatch an error if taker satoshis bigger than maxTakerSatoshis
  if (takerInputSatoshis > maxTakerSatoshis) {
    const valueBCH = Big(maxTakerSatoshis).div(SATOSHIS_IN_BCH);
    const valueUSD = valueBCH.mul(startPrice).div(100).round(2, 0);
    dispatch(
      failed({
        key: ERROR_KEYS.CONTRACT_AMOUNT_GREATER_MAXIMUM,
        field: 'amount',
        params: {
          value:
            currency === 'USD'
              ? `$${valueUSD.toString()}`
              : `${valueBCH.toString()} BCH`,
        },
      })
    );
    if (isModalOpen) {
      dispatch(closeModal({ type: 'longHedgeConfirm' }));
    }
    return;
  }

  // Dispatch an error if taker satoshis smaller than minTakerSatoshis
  if (takerInputSatoshis < minTakerSatoshis) {
    const minUserBCH = Big(minTakerSatoshis)
      .div(SATOSHIS_IN_BCH)
      .mul(startPrice)
      .round(0, 3)
      .div(startPrice)
      .round(8, 3);
    const minUserUSD = minUserBCH.mul(startPrice).div(100).toFixed(2);
    dispatch(
      failed({
        key: ERROR_KEYS.CONTRACT_AMOUNT_BELOW_MINIMUM,
        field: 'amount',
        params: {
          value:
            currency === 'USD'
              ? `$${minUserUSD}`
              : `${minUserBCH.toString()} BCH`,
        },
      })
    );
    if (isModalOpen) {
      dispatch(closeModal({ type: 'longHedgeConfirm' }));
    }
    return;
  }

  const premium = position === HEDGE ? longPremium : hedgePremium;
  let signedTransaction;

  let premiumSatoshis = Number(
    Big(makerInputSatoshis).mul(premium).round(0, 3)
  );

  if (Math.abs(premiumSatoshis) < DUST_SATOSHIS) {
    premiumSatoshis = 0;
  }

  // Calculate the fee to charge the user for using Detoken
  const feeChargedSatoshis = Big(fee)
    .div(100)
    .times(takerInputSatoshis)
    .round(0, 3)
    .toString();

  let totalContractCost = Big(takerInputSatoshis)
    .plus(dustCost)
    .plus(minerCost)
    .plus(feeChargedSatoshis);
  if (premiumSatoshis > 0) {
    totalContractCost = totalContractCost.plus(premiumSatoshis);
  }

  if (Big(balance).mul(SATOSHIS_IN_BCH).lt(totalContractCost)) {
    /* const valueBCH = Big(balance)
      .mul(SATOSHIS_IN_BCH)
      .plus(takerInputSatoshis)
      .minus(totalContractCost)
      .div(SATOSHIS_IN_BCH)
      .round(BCH_DP, 0);

    const valueUSD = valueBCH.mul(startPrice).div(100).toFixed(2);
    add a max button or put this into the form field
    or get this 100% accurate (no estimation) */
    dispatch(
      failed({
        key: ERROR_KEYS.INSUFFICIENT_FUNDS,
        field: 'amount',
        // params: {
        //  value:
        //    currency === 'USD' ? `$${valueUSD}` : `${valueBCH.toString()} BCH`,
        // },
      })
    );
    if (isModalOpen) {
      dispatch(closeModal({ type: 'longHedgeConfirm' }));
    }
    return;
  }

  const UTXOs = UTXOsSelector(state);
  let usedUTXOs = [];
  // Create a transaction
  try {
    const {
      transaction,
      usedUTXOs: UTXOsToRemove,
    } = createPartialFundingTransaction({
      premiumSatoshis,
      takerInputSatoshis,
      makerInputSatoshis,
      dustCost,
      minerCost,
      contractAddress,
      changeAddress,
      productFee: fee,
      anyHedgeFeeAddress: feeAddress,
      UTXOs,
      feeChargedSatoshis: Number(feeChargedSatoshis),
    });

    usedUTXOs = UTXOsToRemove;

    signedTransaction = signPartialFundingTransaction({
      transaction,
      UTXOs: UTXOs || {},
      addressToKey: {
        [activeAddress]: rawActivePrivateKey,
        [changeAddress]: rawChangePrivateKey,
      },
    });
  } catch (e) {
    dispatch(failed({ key: e.message, field: 'amount' }));
    return;
  }

  dispatch(
    saveContractDataAction({
      takerSide: position,
      takerPubKey: activePublicKey,
      hedgeUnits: Number(hedgeUnits),
      contractAddress: contract.address,
      fundingTransaction: signedTransaction,
      id: productId,
      usedUTXOs,
    })
  );

  dispatch(
    openModal({
      type: 'longHedgeConfirm',
      data: {
        startPrice: metadata.startPrice,
        lowLiquidationMulti,
        maturityModifier: metadata.maturityModifier,
        value: Big(takerInputSatoshis).div(SATOSHIS_IN_BCH).toString(),
        fee,
        position,
        premium,
        hedgeInputSats,
        longInputSats,
        dustCost,
        minerCost,
        premiumSatoshis,
        startBlockHeight: metadata.startBlockHeight,
        contractAddress,
        lowLiquidationPrice: parameters.lowLiquidationPrice,
        highLiquidationPrice: parameters.highLiquidationPrice,
      },
    })
  );
  dispatch(success());
};

export interface ISetMaxAmount {
  position: POSITION;
  productId: typeof CONTRACTS[number];
}

export const setMaxAmount = ({ position, productId }: ISetMaxAmount) => async (
  dispatch: Dispatch<Action>,
  getState: () => IState
) => {
  const { HEDGE } = POSITION;
  const state = getState();
  const { futuresList } = state.app;
  const balance = walletBalanceSelector(state); // in BCH here
  const { currency, prices, premiums, maximums } = state.dashboardPage;
  const { hedgePrice, longPrice } = prices;
  const { hedgePremium, longPremium } = premiums;
  const { maxHedgeSatoshis, maxLongSatoshis } = maximums;
  const oraclePrice = oraclePriceSelector(state);
  const {
    lowLiquidationMulti,
    highLiquidationMulti,
    maturityModifier,
    fee,
    minHedgeSatoshis,
    minLongSatoshis,
  } = futuresList.find(({ id }) => id === productId) as IFuture;
  const margin = getMargin(lowLiquidationMulti);

  if (
    balance
      .times(SATOSHIS_IN_BCH)
      .lt(position === HEDGE ? minHedgeSatoshis : minLongSatoshis)
  ) {
    dispatch(
      failed({
        key: ERROR_KEYS.INSUFFICIENT_FUNDS,
        field: 'amount',
      })
    );
  }

  const startPrice = position === HEDGE ? hedgePrice : longPrice;
  const hedgeUnits =
    position === HEDGE
      ? Big(balance)
          .times(hedgePrice || 0)
          .round(0, 0)
          .toString()
      : Big(balance)
          .times(longPrice || 0)
          .round(0, 0)
          .times(margin)
          .toString();

  let contract = null;
  try {
    contract = await manager.createContract(
      '',
      '',
      '',
      Number(hedgeUnits), // expected number in BCH
      startPrice || 0, // expected number
      0,
      0,
      maturityModifier,
      Number(highLiquidationMulti), // expected number
      Number(lowLiquidationMulti) // expected number
    );
  } catch (e) {
    dispatch(
      addSnackbarAction({ message: 'e.something_wrong', variant: 'error' })
    );
    return;
  }
  const { metadata } = contract;
  const { longInputSats, hedgeInputSats, minerCost, dustCost } = metadata;

  const takerInputSatoshis =
    position === HEDGE ? hedgeInputSats : longInputSats;
  const makerInputSatoshis =
    position === HEDGE ? longInputSats : hedgeInputSats;

  const premium = position === HEDGE ? longPremium : hedgePremium;
  let premiumSatoshis = Number(
    Big(makerInputSatoshis)
      .mul(premium || 0)
      .round(0, 3)
  );

  if (Math.abs(premiumSatoshis) < DUST_SATOSHIS) {
    premiumSatoshis = 0;
  }

  // Calculate the fee to charge the user for using Detoken
  const feeChargedSatoshis = Big(fee)
    .div(100)
    .times(takerInputSatoshis)
    .round(0, 3)
    .toString();

  let maxAmount;
  maxAmount = Big(balance)
    .mul(SATOSHIS_IN_BCH)
    .minus(dustCost)
    .minus(minerCost)
    .minus(feeChargedSatoshis);

  if (premiumSatoshis > 0) {
    maxAmount = maxAmount.minus(premiumSatoshis);
  }

  const maxMMTakerSatoshis =
    position === HEDGE
      ? Big(maxLongSatoshis || 0)
          .times(margin)
          .round(0, 3)
      : Big(maxHedgeSatoshis || 0)
          .div(margin)
          .round(0, 3);

  if (maxAmount.gt(maxMMTakerSatoshis)) {
    maxAmount = maxMMTakerSatoshis;
  }

  if (currency === 'USD') {
    maxAmount = maxAmount
      .div(SATOSHIS_IN_BCH)
      .mul(oraclePrice)
      .round(2, 0)
      .toString();
  } else {
    maxAmount = maxAmount.div(SATOSHIS_IN_BCH).round(BCH_DP, 0).toString();
  }
  dispatch(setAmountAction(maxAmount));
};

// Async Actions
export default createReducer<IDashboardPageState>(
  {
    [loading.toString()]: (state: IDashboardPageState) => ({
      ...state,
      isLoading: true,
    }),

    [success.toString()]: (state: IDashboardPageState) => ({
      ...state,
      isLoading: false,
    }),

    [failed.toString()]: (state: IDashboardPageState, error: IError) => ({
      ...state,
      error,
      isLoading: false,
    }),

    [setError.toString()]: (
      state: IDashboardPageState,
      error: IError | null
    ) => ({
      ...state,
      error,
      isLoading: false,
    }),
    [updatePremiumsPricesAction.toString()]: (
      state: IDashboardPageState,
      data: IPremiumsPrices
    ) => ({
      ...state,
      premiums: data.premiums,
      prices: data.prices,
      maximums: data.maximums,
    }),
    [saveContractDataAction.toString()]: (
      state: IDashboardPageState,
      contractData: IContractData
    ) => ({
      ...state,
      contractData,
    }),
    [setAmountAction.toString()]: (
      state: IDashboardPageState,
      amount: string
    ) => ({
      ...state,
      amount,
    }),
    [setCurrencyAction.toString()]: (
      state: IDashboardPageState,
      currency: string
    ) => ({
      ...state,
      currency,
    }),
  },
  DEFAULT_STATE
);
