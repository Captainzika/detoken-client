import Big from 'big.js';
import { IAppState } from '~dux/app/appDux';
import { IError } from '~types/Error';
import {
  IDashboardPageState,
  IContractInfoProps,
  ISetMaxAmount,
} from './dashboardPageDux';
import { CONTRACTS } from '~config/constants';

export interface IMapState {
  oraclePrice: Big;
  balance: Big;
  isLoading: boolean;
  contracts: IAppState['futuresList'];
  error: IDashboardPageState['error'];
  premiums: IDashboardPageState['premiums'];
  amount: IDashboardPageState['amount'];
  prices: IDashboardPageState['prices'];
  currency: IDashboardPageState['currency'];
  isActivePosition: boolean;
}

export interface IMapDispatch {
  getWalletData: (props: {
    activeAddress: string;
    changeAddress: string;
  }) => void;
  setError: (props: IError | null) => void;
  showContractInfo: (props: IContractInfoProps) => void;
  getPremiumPriceData: (productId: typeof CONTRACTS[number]) => void;
  setAmount: (amount: string) => void;
  setCurrency: (currency: string) => void;
  setMaxAmount: ({ position, productId }: ISetMaxAmount) => void;
}

export interface IDashboardPageProps extends IMapState, IMapDispatch {}
