import { connect } from 'react-redux';
import { DashboardPage } from './DashboardPage';
import { IState } from '~dux/rootDux';
import {
  getWalletData,
  setError,
  showContractInfo,
  getPremiumPriceData,
  setAmount,
  setCurrency,
  setMaxAmount,
} from './dashboardPageDux';
import { IMapState, IMapDispatch } from './types';
import { walletBalanceSelector } from '~dux/wallet/walletDux';
import { oraclePriceSelector } from '~dux/app/appDux';
import { isActivePositionSelector } from '~pages/PositionsPage/positionsPageDux';

const mapDispatchToProps: IMapDispatch = {
  getWalletData,
  showContractInfo,
  setError,
  getPremiumPriceData,
  setAmount,
  setCurrency,
  setMaxAmount,
};

const mapStateToProps = (state: IState): IMapState => ({
  oraclePrice: oraclePriceSelector(state),
  balance: walletBalanceSelector(state),
  isActivePosition: isActivePositionSelector(state),
  isLoading: state.dashboardPage.isLoading,
  contracts: state.app.futuresList,
  error: state.dashboardPage.error,
  premiums: state.dashboardPage.premiums,
  amount: state.dashboardPage.amount,
  prices: state.dashboardPage.prices,
  currency: state.dashboardPage.currency,
});

export default connect(mapStateToProps, mapDispatchToProps)(DashboardPage);
