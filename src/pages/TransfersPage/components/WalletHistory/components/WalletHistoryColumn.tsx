import React, { FC } from 'react';
import { fromUnixTime } from 'date-fns/esm';
import { useTranslation } from 'react-i18next';
import Big from 'big.js';
import styled, { Theme } from '~config/theme';
import { CURRENCY_SYMBOL } from '~config/constants';
import { ITransfersPageProps } from '../../../types';

import { IAppState } from '~dux/app/appDux';
import { formatDateWithTranslation } from '~utils/translations';
import ZoomOutSvg from '~icons/zoomOut.svg';
import { Scroll } from '~components';

interface IProps {
  currency: IAppState['userCurrency'];
  onClick: (e: React.MouseEvent<HTMLSpanElement, MouseEvent>) => void;
  data: ITransfersPageProps['deposits'] | ITransfersPageProps['withdrawals'];
  exchangeRates: ITransfersPageProps['exchangeRates'];
  oraclePrice: ITransfersPageProps['oraclePrice'];
  stringSymbol: string;
}

interface IItem {
  padding?: boolean;
  first?: boolean;
  last?: boolean;
  theme: Theme;
  secondary?: boolean;
}

interface ITableHeader {
  padding?: boolean;
  first?: boolean;
  last?: boolean;
  theme: Theme;
}

export const WalletHistoryColumn: FC<IProps> = ({
  currency,
  oraclePrice,
  onClick,
  data,
  exchangeRates,
  stringSymbol,
}: IProps): React.ReactElement => {
  const { t } = useTranslation();

  return data.length ? (
    <Scroll autoHideBar>
      <Table>
        <thead>
          <tr>
            <TableHeader first>{t('amount')}</TableHeader>
            <TableHeader padding>{t('date')}</TableHeader>
            <TableHeader last>&nbsp;</TableHeader>
          </tr>
        </thead>
        <tbody>
          {data
            .sort((a, b) => (a.time >= b.time ? -1 : 1))
            .map(({ txid, amount, time, address }) => (
              <tr key={txid}>
                <Item first key="amount-cell">
                  <span>
                    {stringSymbol}{' '}
                    <span>{Big(amount).round(8, 0).toFixed(8)}</span>{' '}
                    <span>BCH</span>
                  </span>
                  &nbsp;
                  <FiatAmount>
                    ~ {CURRENCY_SYMBOL[currency]}
                    {Big(amount)
                      .mul(oraclePrice)
                      .mul(exchangeRates[currency])
                      .round(2, 0)
                      .toFixed(2)}
                  </FiatAmount>
                </Item>
                <Item padding secondary key="data-cell">
                  {formatDateWithTranslation(fromUnixTime(time))}
                </Item>
                <Item padding last key="modal-opener">
                  <ModalOpener
                    data-amount={amount}
                    data-time={time}
                    data-txid={txid}
                    data-address={address}
                    onClick={onClick}
                  />
                </Item>
              </tr>
            ))}
        </tbody>
      </Table>
    </Scroll>
  ) : (
    <NoData>{t('no_transactions_yet')}</NoData>
  );
};

const ModalOpener = styled<any>(ZoomOutSvg)`
  width: 16px;
  height: 16px;
  margin-left: 6px;
  fill: ${({ theme }): string => theme.text.primary};
  &:hover {
    cursor: pointer;
  }
`;

const FiatAmount = styled.span`
  color: ${({ theme }): string => theme.text.fiat};
`;

const Table = styled.table`
  width: 100%;
  position: relative;
  border-collapse: collapse;
  text-align: left;
  & th,
  & td {
    white-space: nowrap;
  }
  tr:first-of-type td {
    padding-top: 20px;
  }
`;

const TableHeader = styled.th`
  background: ${({ theme }): string => theme.background.secondary};
  font-weight: normal;
  position: sticky;
  top: 0;
  padding-left: ${({ padding }: ITableHeader): string =>
    padding ? '25px' : ''};
  ${({ first }: ITableHeader): string => (first ? 'padding-left: 20px' : '')};
  ${({ last }: ITableHeader): string =>
    last ? 'padding-right: 20px; text-align: right;' : ''};
  color: ${({ theme }): string => theme.text.fiat};

  &:after {
    content: '';
    position: absolute;
    left: 0;
    width: 100%;
  }

  &:after {
    bottom: 1px;
    border-bottom: 1px solid ${({ theme }): string => theme.border.section};
  }

  ${({ theme }): string => `
    ${theme.mediaTablet}{
      background: ${theme.background.primary};
      &:after {
        border-bottom: 1px solid ${theme.border.default};
      }
    }
  `}
`;

const Item = styled.td`
  padding-left: ${({ padding }: IItem): string => (padding ? '25px' : '')};
  ${({ first }: IItem): string =>
    first ? 'padding-left: 20px; display: flex; align-items: center;' : ''};
  ${({ last }: IItem): string =>
    last ? 'padding-right: 20px; text-align: right;' : ''};
  color: ${({ theme }): string => theme.text.primary};
  padding-top: 7px;
  padding-bottom: 8px;
  line-height: 21px;
`;

const NoData = styled.div`
  color: ${({ theme }): string => theme.text.secondary};
  padding: 10px;
  height: 100%;
  align-items: center;
  display: flex;
  justify-content: center;
`;
