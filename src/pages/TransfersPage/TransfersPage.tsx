import React, { FC, ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { ITransfersPageProps } from './types';
import { WalletHistory } from './components/WalletHistory';
import { SendReceiveSection } from '~components/SendReceiveSection';
import { PageTitle } from '~components';

export const TransfersPage: FC<ITransfersPageProps> = ({
  exchangeRates,
  oraclePrice,
  deposits,
  withdrawals,
  currency,
  openModal,
}: ITransfersPageProps): ReactElement => {
  const { t } = useTranslation();
  return (
    <>
      <PageTitle title={`Detoken: ${t('transfers_page')}`} />
      <SendReceiveSection />
      <WalletHistory
        currency={currency}
        openModal={openModal}
        deposits={deposits}
        withdrawals={withdrawals}
        oraclePrice={oraclePrice}
        exchangeRates={exchangeRates}
      />
    </>
  );
};
