import React, { useState, useEffect, FC, useCallback } from 'react';
import { useHistory, useLocation, Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import storage from 'local-storage-fallback';
import { ResetPassword } from './components/ResetPassword';
import { Set2FA } from './components/Set2FA';
import { SuccessReset } from './components/SuccessReset';
import { ISigninPageState, ICheck2FAStatus, ILoginUser } from './signinPageDux';
import {
  TextField,
  Form,
  FormLayout,
  PageTitle,
  LinksDot,
  LinksContainer,
} from '~components';
import { STORAGE_KEYS } from '~config/constants';

interface IProps extends ISigninPageState {
  check2FAStatus: ({ email, password }: ICheck2FAStatus) => void;
  loginUser: ({ email, password, code }: ILoginUser) => void;
  isAuthorized: boolean;
  twoFAMode: boolean | null;
  sendResetPassword: (email: string) => void;
  resetErrors: () => void;
  userEmail: string;
  checkJurisdiction: () => void;
}

export const SigninPage: FC<IProps> = ({
  twoFAMode,
  error,
  loginUser,
  isAuthorized,
  sendResetPassword,
  passwordResetStatus,
  resetErrors,
  loading,
  checkJurisdiction,
}: IProps): React.ReactElement => {
  const userEmail = storage.getItem(STORAGE_KEYS.USER_EMAIL) || '';
  const [email, setEmail] = useState<string>(userEmail);
  const [password, setPassword] = useState<string>('');
  const [twoFACode, setTwoFACode] = useState<string>('');
  const [showReset, setShowReset] = useState(false);
  const [showResetSuccess, setShowResetSuccess] = useState('');
  const history = useHistory();
  const location = useLocation();
  const { t } = useTranslation();

  const { key, field } = error || {};
  const errorText = key ? t(key, { field }) : '';

  useEffect(() => {
    const { state } = location;
    if (isAuthorized) {
      const { from } = (state as any) || { from: { pathname: '/dashboard' } };
      history.push(from);
    }
    if (passwordResetStatus) {
      setShowResetSuccess(passwordResetStatus);
      setShowReset(false);
    }
  }, [isAuthorized, history, location, passwordResetStatus]);

  useEffect(() => {
    checkJurisdiction();
  }, [checkJurisdiction]);

  const handleInputChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>): void => {
      const { name, value } = e.currentTarget;
      resetErrors();
      switch (name) {
        case 'email':
          setEmail(value);
          break;
        case 'password':
          setPassword(value);
          break;
        default:
          break;
      }
    },
    [resetErrors]
  );

  const handle2FAChange = useCallback(
    (value: string): void => {
      setTwoFACode(value);
      resetErrors();
    },
    [resetErrors]
  );
  const handleFormSubmit = useCallback(
    (e: React.FormEvent): void => {
      e.preventDefault();
      loginUser({ email, password, code: twoFACode });
    },
    [email, password, twoFACode, loginUser]
  );

  const sendData = useCallback((): void => {
    loginUser({ email, password, code: twoFACode });
  }, [email, password, twoFACode, loginUser]);

  const handleResetSubmit = useCallback(() => {
    setShowReset(false);
    resetErrors();
  }, [resetErrors]);

  const handleReset = useCallback(
    (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>): void => {
      e.preventDefault();
      resetErrors();
      setShowReset(true);
    },
    [setShowReset, resetErrors]
  );

  if (showReset)
    return (
      <>
        <PageTitle title={`Detoken: ${t('reset_password_page')}`} />
        <ResetPassword
          error={error}
          onSubmit={sendResetPassword}
          onBackButton={handleResetSubmit}
        />
      </>
    );
  if (showResetSuccess) return <SuccessReset status={showResetSuccess} />;

  if (twoFAMode) {
    return (
      <>
        <PageTitle title={`Detoken: ${t('login_2fa_page')}`} />
        <Set2FA
          value={twoFACode}
          onChange={handle2FAChange}
          error={error}
          onSubmit={sendData}
          loading={loading}
        />
      </>
    );
  }

  return (
    <>
      <PageTitle title={`Detoken: ${t('signin_page')}`} />
      <FormLayout
        logo
        title={t('sign_in')}
        subtitle={t('start_trading_instantly')}
      >
        <Form
          onSubmit={handleFormSubmit}
          submitText={t('sign_in_button')}
          submitDisabled={loading}
        >
          <TextField
            name="email"
            type="email"
            value={email}
            placeholder={t('email')}
            onChange={handleInputChange}
            error={field === 'email'}
            helperText={field === 'email' ? errorText : ''}
          />
          <TextField
            name="password"
            type="password"
            value={password}
            placeholder={t('password')}
            onChange={handleInputChange}
            error={field === 'password'}
            helperText={field === 'password' ? errorText : ''}
            inputProps={{ autoFocus: !!email }}
          />
        </Form>
        <LinksContainer>
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a href="#" onClick={handleReset}>
            {t('forgot_password_question')}
          </a>
          <LinksDot />
          <Link to="/signup">{t('dont_have_wallet_question')}</Link>
        </LinksContainer>
      </FormLayout>
    </>
  );
};
