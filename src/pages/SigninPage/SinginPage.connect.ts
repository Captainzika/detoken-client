import { connect } from 'react-redux';
import { SigninPage } from './SigninPage';
import { IState } from '~dux/rootDux';
import {
  check2FAStatus,
  loginUser,
  resetPassword,
  resetErrors,
  checkJurisdiction,
} from './signinPageDux';

const mapDispatchToProps = {
  check2FAStatus,
  loginUser,
  sendResetPassword: resetPassword,
  resetErrors,
  checkJurisdiction,
};

const mapStateToProps = (state: IState) => ({
  isAuthorized: state.user.isAuthorized,
  twoFAMode: state.user.securityFields.twoFaMode,
  ...state.signinPage,
});

export default connect(mapStateToProps, mapDispatchToProps)(SigninPage);
