import { useTranslation } from 'react-i18next';
import React, { FC, useCallback } from 'react';
import { STEPS } from '../types';
import { FormLayout, Button } from '~components';
import styled from '~config/theme';

interface IProps {
  onBackButton: (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => void;
  onFormSubmit: React.Dispatch<React.SetStateAction<STEPS>>;
}

const SignupSeedWordsNoteStep: FC<IProps> = ({
  onBackButton,
  onFormSubmit,
}: IProps): React.ReactElement => {
  const { t } = useTranslation();

  const handleFormSubmit = useCallback(
    (e: React.FormEvent<Element>) => {
      e.preventDefault();
      onFormSubmit(STEPS.CREATE);
    },
    [onFormSubmit]
  );

  return (
    <FormLayout logo onBackClick={onBackButton} title={t('seed_words_title')}>
      <Text>{t('seed_words_keep_private_note')}</Text>
      <Button fullWidth onClick={handleFormSubmit}>
        {t('seed_words_button')}
      </Button>
    </FormLayout>
  );
};

const Text = styled.p`
  margin-top: 0;
  margin-bottom: 30px;
`;

export { SignupSeedWordsNoteStep };
