import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';
import styled from '~config/theme';
import EmailSvg from '~icons/email.svg';
import { FormLayout } from '~components';

const SuccessStep: FC = (): React.ReactElement => {
  const { t } = useTranslation();

  return (
    <FormLayout
      logo
      title={t('success_step_title')}
      subtitle={t('success_step_subtitle')}
      icon={<EmailLogo />}
    >
      {null}
    </FormLayout>
  );
};

const EmailLogo = styled<any>(EmailSvg)`
  width: 80px;
  height: 80px;
  fill: ${({ theme }): string => theme.logo.secondaryColor};
`;

export { SuccessStep };
