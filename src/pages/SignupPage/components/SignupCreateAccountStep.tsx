import { useTranslation } from 'react-i18next';
import React, { FC, useState, useCallback } from 'react';
import { FormLayout, Form, InputWrapper, CheckBox } from '~components';

import styled from '~config/theme';

interface IProps {
  seed: string;
  onFormSubmit: () => void;
  onBackButton: () => void;
}

const SignupCreateAccountStep: FC<IProps> = ({
  seed,
  onFormSubmit,
  onBackButton,
}: IProps): React.ReactElement => {
  const [error, setError] = useState(false);
  const [isUnderstand, setIsUnderstand] = useState(false);
  const { t } = useTranslation();

  const handleInputChange = useCallback((): void => {
    setIsUnderstand(!isUnderstand);
    setError(false);
  }, [setError, isUnderstand]);

  const handleFormSubmit = useCallback(
    (e: React.FormEvent): void => {
      e.preventDefault();
      if (!isUnderstand) {
        setError(true);
        return;
      }
      onFormSubmit();
      setError(false);
    },
    [onFormSubmit, setError, isUnderstand]
  );

  return (
    <FormLayout
      logo
      onBackClick={onBackButton}
      title={t('seed_words_title')}
      subtitle={t('backup_words_subtitle')}
    >
      <Form onSubmit={handleFormSubmit} submitText={t('continue_button')}>
        <SeedWords>{seed}</SeedWords>
        <Text>{t('seed_words_keep_restore_note')}</Text>
        <InputWrapper error={error}>
          <InputContainer>
            <CheckBox
              handleClick={handleInputChange}
              isChecked={isUnderstand}
              error={error}
            />
            <CheckboxLabel onClick={handleInputChange}>
              {t('understand_backup_seed')}
            </CheckboxLabel>
          </InputContainer>
        </InputWrapper>
      </Form>
    </FormLayout>
  );
};

const Text = styled.p`
  margin: 30px 0;
  text-align: center;
`;

const SeedWords = styled.div`
  margin-top: 0;
  width: 100%;
  max-width: 100%;
  border: 3px solid ${({ theme }): string => theme.border.copySeedWords};
  border-radius: 3px;
  background-color: transparent;
  font-size: 1rem;
  text-align: center;
  padding: 10px;
  resize: none;
  box-sizing: border-box;
  color: ${({ theme }): string => theme.text.primary};
`;

const InputContainer = styled.div`
  display: flex;
  align-items: center;
`;

const CheckboxLabel = styled.span`
  cursor: pointer;
`;

export { SignupCreateAccountStep };
