import React, { FC, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import styled from '~config/theme';
import { STEPS } from '../types';
import { IError } from '~types/Error';
import { FormLayout, Button } from '~components';
import LockSvg from '~icons/lock.svg';

type StepType = STEPS.INIT_2FA | STEPS.NO_2FA;

interface IProps {
  error: IError | null;
  isLoading: boolean;
  onFormSubmit: (step: StepType) => void;
  onBackButton: () => void;
}

const Signup2FAOrNotStep: FC<IProps> = ({
  isLoading,
  onFormSubmit,
  onBackButton,
}: IProps): React.ReactElement => {
  const { t } = useTranslation();

  const handleButtonClick = useCallback(
    (e: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
      const step = e.currentTarget.dataset.step as StepType;
      onFormSubmit(step);
    },
    [onFormSubmit]
  );

  return (
    <FormLayout
      logo
      title={t('two_fa_authentication_title')}
      subtitle={t('two_fa_authentication_subtitle')}
      onBackClick={onBackButton}
      icon={<LockIcon />}
    >
      <Button
        onClick={handleButtonClick}
        data-step={STEPS.INIT_2FA}
        fullWidth
        disabled={isLoading}
      >
        {t('use_2fa')}
      </Button>

      <StyledButton
        onClick={handleButtonClick}
        data-step={STEPS.NO_2FA}
        fullWidth
        variant="link"
        disabled={isLoading}
      >
        {t('do_not_use_2fa')}
      </StyledButton>
    </FormLayout>
  );
};

const StyledButton = styled(Button)`
  margin-top: 30px;
  height: min-content;
`;

const LockIcon = styled<any>(LockSvg)`
  fill: ${({ theme }): string => theme.logo.secondaryColor};
`;

export { Signup2FAOrNotStep };
