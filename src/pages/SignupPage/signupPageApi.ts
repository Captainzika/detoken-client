import request from '~providers/helpers';
import { IError } from '~types/Error';

export interface ICreateUserProps {
  name: string;
  email: string;
  passwordHash: string;
  encryptedMnemonic: string;
  sin: string;
  twoFaSecret?: string;
  code?: string;
  refId?: string;
}

export const createUser = async (
  props: ICreateUserProps
): Promise<IError | void> => request.post('/users', props);
