import { connect } from 'react-redux';
import { SignupPage } from './SignupPage';
import {
  signup,
  triggerErrorAction,
  signupPageLoadingSelector,
  signupPageErrorsSelector,
  signupPageSuccessSelector,
  ISignupPageState,
} from './signupPageDux';
import { IState } from '~dux/rootDux';

const mapDispatchToProps = {
  signup,
  triggerErrorAction,
};

const mapStateToProps = (state: IState): ISignupPageState => ({
  isLoading: signupPageLoadingSelector(state),
  success: signupPageSuccessSelector(state),
  error: signupPageErrorsSelector(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(SignupPage);
