import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { RootElement } from './components/RootElement';

ReactDOM.render(<RootElement />, document.getElementById('root'));
