export interface IUTXO {
  txid: string;
  outputIndex: number;
  address: string;
  amount: number;
  scriptPubKey: string;
}

export interface IUTXOs {
  [key: string]: IUTXO;
}

export interface ITransaction {
  txid: string;
  time: number;
  amount: string;
  address?: string;
}

export interface ITransactions {
  sent: ITransaction[];
  received: ITransaction[];
}

export interface IListUnspentMessage {
  [address: string]: Array<IUTXOMessage>;
}

export interface IUTXOMessage {
  tx_hash: string;
  tx_pos: number;
  height: number;
  value: number;
  script: string;
}

export interface IHistoryPayload {
  history: IHistoryRes;
  activeAddress: string;
  changeAddress: string;
  contracts: string[];
}

export interface ITransactionPayload {
  transaction: ITransactionRes;
  address: string;
}

export interface IHistoryRes {
  [address: string]: Array<ITransactionRes>;
}

export interface ITransactionRes {
  time?: number;
  coinbase: string;
  txid: string;
  version: number;
  locktime: number;
  vin: Array<{
    address: string;
    value: string;
  }>;
  vout: Array<{
    value: string;
    n: number;
    scriptPubKey: {
      asm: string;
      hex: string;
      reqSigs: number;
      type: 'pubkeyhash' | 'scripthash';
      addresses: string[];
    };
  }>;
  hex: string;
}
