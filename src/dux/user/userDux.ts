import {
  getPublicKeyFromPrivateKey,
  // @ts-expect-error
} from 'bitauth/lib/bitauth-browserify';
// @ts-expect-error
import PrivateKey from 'bitcore-lib-cash/lib/privatekey';
import { createAction, createReducer } from 'redux-act';
import { createSelector } from 'reselect';
import { IState } from '~dux/rootDux';
import {
  getPrivateKeyFromXpriv,
  getActiveDerivation,
  getChangeDerivation,
} from '~utils/wallet';

export type twoFAModeType = boolean | null;

interface IUserData {
  name: string;
  email: string;
  xpriv: string;
  sin: string;
  securityFields: { twoFaMode: twoFAModeType };
  refCode: string;
  encryptedMnemonic: string;
}

export interface IUserState extends IUserData {
  isAuthorized: boolean;
}
interface IPrivateKeySelector {
  rawActivePrivateKey: string;
  rawChangePrivateKey: string;
  activePrivateKey: string;
  changePrivateKey: string;
}

interface IPublicKeySelector {
  activePublicKey: string;
  changePublicKey: string;
}

interface IAddressesSelector {
  activeAddress: string;
  changeAddress: string;
}

const DEFAULT_STATE = {
  isAuthorized: false,
  name: '',
  email: '',
  xpriv: '',
  sin: '',
  securityFields: { twoFaMode: null },
  refCode: '',
  encryptedMnemonic: '',
};

// Selectors
const duxSelector = (state: IState): IUserState => state.user;

export const getPrivateKeysSelector = createSelector(
  duxSelector,
  ({ xpriv }): IPrivateKeySelector => {
    const rawActivePrivateKey = getPrivateKeyFromXpriv({
      xpriv,
      derivation: getActiveDerivation(),
    });
    const rawChangePrivateKey = getPrivateKeyFromXpriv({
      xpriv,
      derivation: getChangeDerivation(),
    });
    return {
      rawActivePrivateKey,
      rawChangePrivateKey,
      activePrivateKey: rawActivePrivateKey.toString(),
      changePrivateKey: rawChangePrivateKey.toString(),
    };
  }
);

export const getPublicKeysSelector = createSelector(
  getPrivateKeysSelector,
  ({ activePrivateKey, changePrivateKey }): IPublicKeySelector => {
    const activePublicKey = getPublicKeyFromPrivateKey(activePrivateKey);
    const changePublicKey = getPublicKeyFromPrivateKey(changePrivateKey);
    return { activePublicKey, changePublicKey };
  }
);

export const getAddressesSelector = createSelector(
  getPrivateKeysSelector,
  ({ activePrivateKey, changePrivateKey }): IAddressesSelector => ({
    activeAddress: new PrivateKey(activePrivateKey).toAddress().toString(),
    changeAddress: new PrivateKey(changePrivateKey).toAddress().toString(),
  })
);

export const saveUserDataAction = createAction<IUserData>('saveData');

export const saveUserAuthorizationAction = createAction<boolean>(
  'saveAuthorization'
);

export const setUser2FAModeAction = createAction<boolean>(
  'setUser2FAModeAction'
);

export const eraseUserDataAction = createAction('eraseUserDataAction');

export const updateEncryptedMnemonic = createAction<string>(
  'updateEncryptedMnemonic'
);

export default createReducer<IUserState>(
  {
    [saveUserDataAction.toString()]: (state: IUserState, data: IUserData) => ({
      ...state,
      ...data,
      isAuthorized: true,
    }),

    [saveUserAuthorizationAction.toString()]: (
      state: IUserState,
      authorization: boolean
    ) => ({
      ...state,
      isAuthorized: authorization,
    }),

    [setUser2FAModeAction.toString()]: (state: IUserState, mode: boolean) => ({
      ...state,
      securityFields: {
        ...state.securityFields,
        twoFaMode: mode,
      },
    }),

    [eraseUserDataAction.toString()]: () => ({
      ...DEFAULT_STATE,
    }),
    [updateEncryptedMnemonic.toString()]: (
      state: IUserState,
      encryptedMnemonic: string
    ) => ({
      ...state,
      encryptedMnemonic,
    }),
  },
  DEFAULT_STATE
);
