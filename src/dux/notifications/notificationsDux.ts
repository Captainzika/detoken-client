import { createAction, createReducer } from 'redux-act';
import { Dispatch } from 'redux';
import { createSelector } from 'reselect';
import { IState } from '~dux/rootDux';

export enum REASONS {
  CLICKAWAY = 'clickaway',
  MAXSNACK = 'maxsnack',
  INSTRUCTED = 'instructed',
}
type ISnackbarKey = string | number;

type TransitionCloseHandler = (
  event: React.SyntheticEvent<any> | null,
  reason: REASONS,
  key?: ISnackbarKey
) => void;

export interface INotification {
  key: ISnackbarKey;
  message: string;
  onClose?: TransitionCloseHandler;
  entered?: boolean;
  variant?: 'info' | 'success' | 'error';
  dismissed?: boolean;
  keepVisible?: boolean;
}

export type INotificationsState = INotification[];

const DEFAULT_STATE: INotificationsState = [];

// Selectors
const duxSelector = (state: IState): INotificationsState => state.notifications;

export const notificationsSelector = createSelector(
  duxSelector,
  (notifications) => notifications
);

// Sync Actions
export const addSnackbarAction = createAction<Partial<INotification>>(
  'addSnackbarAction'
);
export const closeSnackbarAction = createAction<ISnackbarKey>(
  'closeSnackbarAction'
);
export const removeSnackbarAction = createAction<ISnackbarKey>(
  'removeSnackbarAction'
);

type Action = ReturnType<
  | typeof addSnackbarAction
  | typeof closeSnackbarAction
  | typeof removeSnackbarAction
>;

export const removeSnackbar = (key: ISnackbarKey) => (
  dispatch: Dispatch<Action>
): void => {
  dispatch(removeSnackbarAction(key));
};

export const closeSnackbar = (key: ISnackbarKey) => (
  dispatch: Dispatch<Action>
): void => {
  dispatch(closeSnackbarAction(key));
};

// Async Actions
export default createReducer<INotificationsState>(
  {
    [addSnackbarAction.toString()]: (
      state: INotificationsState,
      notification
    ) => [
      ...state,
      {
        key: notification.key || new Date().getTime() + Math.random(),
        variant: notification.variant || 'info',
        ...notification,
      },
    ],

    [closeSnackbarAction.toString()]: (state: INotificationsState, payload) =>
      state.map((notification) =>
        !notification.key || notification.key === payload
          ? { ...notification, dismissed: true }
          : { ...notification }
      ),

    [removeSnackbarAction.toString()]: (state: INotificationsState, payload) =>
      payload
        ? state.filter((notification) => notification.key !== payload)
        : DEFAULT_STATE,
  },
  DEFAULT_STATE
);
