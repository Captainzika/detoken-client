import styled, { Theme } from '~config/theme';

interface IProps {
  variant?: 'primary' | 'secondary' | 'link';
  theme: Theme;
  fullWidth?: boolean;
}

export const Button = styled.button(
  ({ theme, variant = 'primary', fullWidth }: IProps) => `
  background-color:
    ${variant === 'primary' ? theme.button.color : ''}
    ${variant === 'secondary' ? theme.secondaryButton.color : ''}
    ${variant === 'link' ? theme.linkButton.color : ''};

  color: 
    ${variant === 'primary' ? theme.button.text : ''}
    ${variant === 'secondary' ? theme.secondaryButton.text : ''}
    ${variant === 'link' ? theme.linkButton.text : ''};

  border-radius: 3px;
  border: none;
  height: 38px;
  cursor: pointer;
  padding: 0 18px;
  font-size: 1rem;
  font-family: 'Inter', serif;
  outline: none;
  max-width: 312px;
  &:hover {
    background-color: 
      ${variant === 'primary' ? theme.button.hoverColor : ''}
      ${variant === 'secondary' ? theme.secondaryButton.hoverColor : ''};
    
    ${variant === 'link' ? `color: ${theme.linkButton.hoverColor}` : ''};
    
    text-decoration: ${variant === 'link' ? 'underline' : ''};
  }

  &:active {
    outline: none;
  }

  &:disabled {
    background-color: 
      ${variant === 'primary' ? theme.button.disabledColor : ''}
      ${variant === 'secondary' ? theme.secondaryButton.disabledColor : ''};  
    ${variant === 'link' ? `color: ${theme.linkButton.disabledColor}` : ''};
    
  }
  
  ${fullWidth ? 'width: 100%;' : ''}
  ${variant === 'link' ? 'width: auto; padding: 0;' : ''}

`
);
