import React, { FC, ReactElement } from 'react';
import styled from '~config/theme';
import { Input } from '~components';
import { IInputProps, InputButton } from '~components/Input/Input';

interface IProps extends IInputProps {
  dropdown: React.ReactNode;
}

export const InputDropdown: FC<IProps> = (props: IProps): ReactElement => {
  const { dropdown, btnText, btnClick, ...rest } = props;
  return (
    <InputWrapper>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <Input rightComponent={<DropWrapper>{dropdown}</DropWrapper>} {...rest} />
      {btnText && (
        <InputButton onClick={btnClick} type="button">
          {btnText}
        </InputButton>
      )}
    </InputWrapper>
  );
};

const InputWrapper = styled.div`
  display: flex;
  & > div {
    height: 48px;
    & > input {
      padding-right: 75px;
      font-size: 24px;
      border-radius: 3px 0 0 3px;
    }
  }
  & > button {
    height: 48px;
  }
`;

const DropWrapper = styled.div`
  position: absolute;
  right: 0;
  top: 50%;
  transform: translateY(-50%);
  & > div {
    margin: 0 5px;
  }
`;
