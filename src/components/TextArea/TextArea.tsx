import styled from '~config/theme';

interface IProps {
  theme: any;
  error?: boolean;
}

export const TextArea = styled.textarea(
  ({ theme, error }: IProps) => `
  background: ${theme.input.background};
  border: 2px solid ${
    error ? theme.input.errorBorder : theme.input.borderColor
  };
  border-radius: 3px;
  box-sizing: border-box;
  color: ${theme.text.primary};
  outline: none;
  width: 100%;
  max-width: 400px;
  height: 38px;
  font-size: 1rem;
  min-height: 150px;
  padding: 10px;
  
  &:focus {
    border-color: ${
      error ? theme.input.focusErrorBorder : theme.input.focusBorder
    };
  }
  
  &:disabled {
    border-color: ${theme.input.disabledBorder};
    color: ${theme.input.disabledText};
  }
  
  &::placeholder {
    color: ${theme.text.secondary} 
  }
 
`
);
