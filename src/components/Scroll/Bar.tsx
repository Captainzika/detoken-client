import React from 'react';
import styled from '~config/theme';

// eslint-disable-next-line react/display-name
export const Bar = React.forwardRef((props, ref) => (
  // @ts-ignore
  // eslint-disable-next-line react/jsx-props-no-spreading
  <ScrollBar ref={ref} {...props} />
));

const ScrollBar = styled.div`
  position: absolute;
  width: 12px;
  top: 0;
  right: 0;
  transition: opacity 200ms ease-out;
  z-index: 101;

  &:after {
    content: '';
    position: absolute;
    background: ${(props) => props.theme.scrollBarColor};
    left: 6px;
    top: 3px;
    right: 3px;
    bottom: 3px;
    border-radius: 3px;
  }
`;
