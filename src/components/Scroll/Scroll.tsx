/* eslint-disable react/default-props-match-prop-types */
/* eslint-disable no-underscore-dangle */
import React from 'react';
import debounce from 'lodash.debounce';
import { Bar as BarDefault } from './Bar';
import styled from '~config/theme';

interface IProps {
  autoHideBar?: boolean;
  trackWidth?: number;
  minBarLength?: number;
  maxHeight?: number;
  Bar?: () => React.ReactElement;
  children: React.ReactNode;
}

const body = document.querySelector('body');

const userSelectDisableCss = `user-select: none`;

export class Scroll extends React.PureComponent<IProps> {
  // eslint-disable-next-line react/static-property-placement
  static defaultProps = {
    trackWidth: 12,
    minBarLength: 18,
    maxHeight: Infinity,
    autoHideBar: false,
    Bar: BarDefault,
  };

  _Content = React.createRef<HTMLDivElement>();
  _Host = React.createRef<HTMLDivElement>();
  _Overflow = React.createRef<HTMLDivElement>();
  _Bar = React.createRef<HTMLDivElement>();
  _Track = React.createRef<HTMLDivElement>();
  _hostH = 0;
  _contentH = 0;
  _barH = 0;
  _barVisTimeout = 0;
  _isMousedown: boolean | null = null;
  _isMouseenter: boolean | null = null;
  _mouseenterTimeout = 0;
  startPos = 0;

  handleWindowResize = debounce(() => {
    window.requestAnimationFrame(this.initDimensions);
  }, 300);

  componentDidMount(): void {
    const { autoHideBar } = this.props;
    if (autoHideBar) {
      this._Bar.current!.style.opacity = '0';
      this._Host.current!.addEventListener('mouseenter', this.handleMouseenter);
      this._Host.current!.addEventListener('mouseleave', this.handleMouseleave);
    }
    // @ts-ignore
    this._Track.current!.addEventListener('mousedown', this.handleMousedown);
    this._Overflow.current!.addEventListener('scroll', this.handleScroll);
    window.addEventListener('resize', this.handleWindowResize);
    this.initDimensions();
  }

  componentDidUpdate(): void {
    this._Content.current!.style.width = '';
    this.initDimensions();
  }

  componentWillUnmount(): void {
    // @ts-ignore
    this._Track.current!.removeEventListener('mousedown', this.handleMousedown);
    this._Host.current!.removeEventListener(
      'mouseenter',
      this.handleMouseenter
    );
    this._Host.current!.removeEventListener(
      'mouseleave',
      this.handleMouseleave
    );
    this._Overflow.current!.removeEventListener('scroll', this.handleScroll);
    window.removeEventListener('resize', this.handleWindowResize);
    clearTimeout(this._barVisTimeout);
  }

  render() {
    const {
      children,
      Bar,
      trackWidth,
      autoHideBar, // eslint-disable-line no-unused-vars
      minBarLength, // eslint-disable-line no-unused-vars
      maxHeight, // eslint-disable-line no-unused-vars
      ...restProps
    } = this.props;

    return (
      // eslint-disable-next-line react/jsx-props-no-spreading
      <Host {...restProps} ref={this._Host}>
        <Overflow ref={this._Overflow}>
          <Content ref={this._Content}>{children}</Content>
        </Overflow>
        <Track
          ref={this._Track}
          style={{ width: `${trackWidth ? `${trackWidth}px` : 0}` }}
        >
          {/* @ts-ignore */}
          <Bar ref={this._Bar} />
        </Track>
      </Host>
    );
  }

  initDimensions = () => {
    const { maxHeight, minBarLength } = this.props;
    const { _Content, _Host, _Overflow, _Bar, _Track } = this;

    if (!_Host.current) return;
    const contentH = _Content.current!.clientHeight;
    if (this._contentH !== contentH) _Overflow.current!.style.height = '';
    const hostH = Math.min(_Host.current.clientHeight, maxHeight!);
    const barH = Math.max((hostH * hostH) / contentH, minBarLength!);
    const needScroll = contentH > hostH;

    this._hostH = hostH;
    this._contentH = contentH;
    this._barH = barH;

    _Overflow.current!.style.height = `${hostH}px`;

    if (!needScroll) {
      _Track.current!.style.visibility = 'hidden';
      _Bar.current!.style.visibility = 'hidden';
    } else {
      _Track.current!.style.visibility = '';
      _Bar.current!.style.visibility = '';
      _Bar.current!.style.height = `${barH}px`;
    }
  };

  handleScroll = () => {
    window.requestAnimationFrame(() => {
      const { autoHideBar } = this.props;
      this._Bar.current!.style.transform = `translateY(${this.getBarPos()}px)`;

      if (autoHideBar) {
        this._Bar.current!.style.opacity = '1';
        clearTimeout(this._barVisTimeout);
        this._barVisTimeout = window.setTimeout(() => {
          if (!this._isMouseenter) {
            this._Bar.current!.style.opacity = '0';
          }
        }, 1000);
      }
    });
  };

  handleMousedown: React.MouseEventHandler = ({ target, pageY, clientY }) => {
    if (target !== this._Bar.current) {
      this.handleTrackMousedown({ clientY });
    }

    this.startPos = pageY - this.getBarPos();
    this._Overflow.current!.removeEventListener('scroll', this.handleScroll);
    // @ts-ignore
    document.addEventListener('mousemove', this.handleMousemove);
    document.addEventListener('mouseup', this.handleMouseup);
    body!.classList.add(userSelectDisableCss);
    this._isMousedown = true;
  };

  handleTrackMousedown = ({ clientY }: { clientY: number }) => {
    const { _hostH, _barH, _Overflow, _Host } = this;
    const y = clientY - _Host.current!.getBoundingClientRect().top;
    const barPos = (_hostH - _barH) * (y / (_hostH - _barH)) - _barH / 2;
    const scrollTop = this.getScrollTop(barPos);
    _Overflow.current!.scrollTop = scrollTop;
    this.moveBar(scrollTop, barPos);
  };

  handleMouseup = () => {
    this._Overflow.current!.addEventListener('scroll', this.handleScroll);
    // @ts-ignore
    document.removeEventListener('mousemove', this.handleMousemove);
    document.removeEventListener('mouseup', this.handleMouseup);
    body!.classList.remove(userSelectDisableCss);
    this._isMousedown = false;
  };

  handleMousemove: React.MouseEventHandler = ({ pageY }) => {
    window.requestAnimationFrame(() => {
      const { _Overflow, getScrollTop } = this;
      const barPos = pageY - this.startPos;
      const scrollTop = getScrollTop(barPos);
      _Overflow.current!.scrollTop = scrollTop;
      this.moveBar(scrollTop, barPos);
    });
  };

  handleMouseenter = () => {
    clearTimeout(this._mouseenterTimeout);
    this._mouseenterTimeout = window.setTimeout(() => {
      this._isMouseenter = true;
      this._Bar.current!.style.opacity = '1';
    }, 100);
    document.removeEventListener('mouseup', this.handleOpacityMouseup);
  };

  handleMouseleave = () => {
    clearTimeout(this._mouseenterTimeout);
    this._isMouseenter = false;
    if (!this._isMousedown) {
      this._Bar.current!.style.opacity = '0';
    } else {
      document.addEventListener('mouseup', this.handleOpacityMouseup);
    }
  };

  handleOpacityMouseup = () => {
    this._Bar.current!.style.opacity = '0';
    document.removeEventListener('mouseup', this.handleOpacityMouseup);
  };

  getBarPos = () => {
    const { _hostH, _contentH, _barH, _Overflow } = this;
    return (
      ((_hostH - _barH) * _Overflow.current!.scrollTop) / (_contentH - _hostH)
    );
  };

  getScrollTop = (barPos: number) => {
    const { _hostH, _contentH, _barH } = this;
    return (barPos * (_contentH - _hostH)) / (_hostH - _barH);
  };

  moveBar = (scrollTop: number, barPos: number) => {
    const { _hostH, _barH, _Bar, _contentH } = this;

    if (scrollTop < 0) {
      _Bar.current!.style.transform = `translateY(0)`;
    } else if (scrollTop >= _contentH - _hostH) {
      _Bar.current!.style.transform = `translateY(${_hostH - _barH}px)`;
    } else {
      _Bar.current!.style.transform = `translateY(${barPos}px)`;
    }
  };
}

const Host = styled.div`
  overflow: hidden;
  position: relative;
  height: 100%;
`;

const Overflow = styled.div`
  position: relative;
  overflow: auto;
  -webkit-overflow-scrolling: touch;
  padding-right: 25px;
  margin-right: -25px;
  scrollbar-width: none;
`;

const Content = styled.div`
  position: relative;
`;

const Track = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
`;
