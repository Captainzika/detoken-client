import React, { FC, ReactElement, useCallback } from 'react';
import styled, { Theme } from '~config/theme';

interface ICheckboxProps {
  isChecked: boolean;
  disabled?: boolean;
  handleClick: () => void;
  className?: string;
  error?: boolean;
}

export const CheckBox: FC<ICheckboxProps> = ({
  isChecked,
  error,
  className = '',
  disabled,
  handleClick,
}: ICheckboxProps): ReactElement => {
  const uniqueId = `${Math.random()}${Date.now()}`;

  const handleOnClick = useCallback(
    (event: React.MouseEvent<HTMLElement>) => {
      event.stopPropagation();
      event.preventDefault();
      handleClick();
    },
    [handleClick]
  );

  return (
    <CheckboxWrapper
      isChecked={isChecked}
      disabled={disabled}
      className={className}
    >
      <HiddenInput
        type="checkbox"
        id={uniqueId}
        disabled={disabled}
        checked={isChecked}
        tabIndex={-1}
        readOnly
      />
      <CheckboxMask onClick={handleOnClick} tabIndex={-1} />
      <CheckboxLabel
        htmlFor={uniqueId}
        isChecked={isChecked}
        error={error}
        tabIndex={0}
        onKeyPress={handleClick}
        onClick={handleOnClick}
      />
    </CheckboxWrapper>
  );
};

const CheckedStyles = (theme: Theme): string => `
  border-color: ${theme.primaryAppColor};
  &:after {
    content: '';
    position: absolute;
    z-index: 1;
    top: 4px;
    left: 2px;
    width: 12px;
    height: 5px;
    border-left: 3px solid ${theme.primaryAppColor};
    border-bottom: 3px solid ${theme.primaryAppColor};
    transform: rotate(-45deg);
    transition: all 0.2s ease-in-out;
  }
`;

interface IStyledProps {
  isChecked: ICheckboxProps['isChecked'];
  disabled?: boolean;
  error?: boolean;
}

const CheckboxWrapper = styled.div<IStyledProps>`
  width: 100%;
  max-width: 24px;
  height: 24px;
  position: relative;
  margin-right: 10px;
  ${({ disabled }: IStyledProps): string =>
    disabled
      ? `
      opacity: 0.5;
      pointer-events: none;
      user-select: none;
  `
      : ''};
`;

const CheckboxMask = styled.div`
  position: absolute;
  cursor: pointer;
  top: 50%;
  left: 50%;
  width: 200%;
  height: 200%;
  transform: translate(-50%, -50%);
  z-index: 10;
  &:focus {
    outline: none;
  }
  &:active {
    & + label {
      border-color: ${({ theme }): string => theme.primaryAppColor};
    }
  }
  -webkit-tap-highlight-color: transparent;
`;

const HiddenInput = styled.input`
  opacity: 0;
  position: absolute;
  top: 0;
  left: -99999px;
  appearance: none;
  z-index: 1;
  &:focus ~ label {
    border-color: ${({ theme }): string => theme.input.borderColor};
  }
`;
const CheckboxLabel = styled.label<IStyledProps>`
  width: 20px;
  height: 20px;
  position: absolute;
  border-radius: 3px;
  border: 2px solid ${({ theme }): string => theme.input.borderColor};
  display: block;
  transition: all 0.2s ease-in-out;
  z-index: 1;
  background: ${({ theme }): string => theme.background.tertiary};
  &:focus {
    ${({ theme }): string =>
      `
        border-color: ${theme.primaryAppColor};
        outline: none;
        border-radius: 3px; 
  `};
  }
  outline: none;
  cursor: pointer;

  ${({ theme, isChecked }): string => (isChecked ? CheckedStyles(theme) : '')};
  ${({ error, theme }): string =>
    error
      ? `
      border-color: ${theme.input.errorBorder}; 
  `
      : ''};
  ${(props): string =>
    props.disabled
      ? `
    border-color: ${props.theme.input.disabledBorder};
  `
      : ''}
`;
