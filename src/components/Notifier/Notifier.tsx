import React, { FC, ReactNode } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import Transition, {
  TransitionStatus,
} from 'react-transition-group/Transition';
import CloseSvg from '~icons/close.svg';
import styled from '~config/theme';
import { INotifierProps } from './types';
import {
  closeSnackbarAction,
  removeSnackbarAction,
} from '~dux/notifications/notificationsDux';

const TRANSITION_TIMEOUT = 300;
const TIME_TO_HIDE = 10000;

export const Notifier: FC<INotifierProps> = ({
  notifications,
}: INotifierProps) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  return (
    <>
      {notifications.map(
        (
          {
            key,
            message,
            variant = 'info',
            dismissed = false,
            keepVisible = false,
          },
          index
        ) => {
          if (!dismissed && !keepVisible) {
            setTimeout(() => {
              dispatch(closeSnackbarAction(key));
            }, TIME_TO_HIDE);
          }

          return (
            <Transition
              key={key}
              in={!dismissed}
              appear
              exit
              timeout={TRANSITION_TIMEOUT}
              unmountOnExit
              onExited={(): void => {
                dispatch(removeSnackbarAction(key));
              }}
            >
              {(state: TransitionStatus): ReactNode => (
                <Container
                  zIndex={index + 1001}
                  duration={TRANSITION_TIMEOUT}
                  state={state}
                  variant={variant}
                >
                  <Text>{t(message)}</Text>
                  <CloseBtn
                    variant={variant}
                    onClick={(): void => {
                      dispatch(closeSnackbarAction(key));
                    }}
                  >
                    <CloseSvg />
                  </CloseBtn>
                </Container>
              )}
            </Transition>
          );
        }
      )}
    </>
  );
};

const transitionStyles: { [key in TransitionStatus]: string } = {
  entering: '1',
  entered: '1',
  exiting: '0',
  exited: '0',
  unmounted: '0',
};

const Container = styled.div<{
  state: TransitionStatus;
  duration: number;
  variant: 'info' | 'error' | 'success';
  zIndex: number;
}>`
  position: fixed;
  max-width: calc(100% - 20px);
  left: 50%;
  transform: translate(-50%, 0%);
  box-sizing: border-box;
  top: 15px;
  width: 400px;
  padding: 8px 15px;
  font-size: 1rem;
  border-radius: 3px;
  z-index: ${({ zIndex }): number => zIndex};
  transition: ${({ duration }): string => `opacity ${duration}ms ease-in-out`};
  opacity: ${({ state }): string => transitionStyles[state]};
  background-color: ${({ theme, variant }): string =>
    theme.snackbar[variant].background};
  color: ${({ theme, variant }): string => theme.snackbar[variant].text};
  border: 1px solid
    ${({ theme, variant }): string => theme.snackbar[variant].border};
  align-items: center;
  display: flex;
  justify-content: space-between;
`;

const Text = styled.div`
  padding: 0 16px;
  flex-grow: 1;
  text-align: center;
`;

const CloseBtn = styled.div<{ variant: 'info' | 'error' | 'success' }>`
  cursor: pointer;
  display: flex;
  position: absolute;
  right: 6px;
  svg {
    width: 16px;
    height: 16px;
    fill: ${({ theme, variant }): string => theme.snackbar[variant].border};
  }
`;
