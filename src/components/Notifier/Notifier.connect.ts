import { connect } from 'react-redux';
import { IState } from '~dux/rootDux';
import { IMapState } from './types';
import { Notifier } from './Notifier';

const mapStateToProps = (state: IState): IMapState => ({
  notifications: state.notifications,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Notifier);
