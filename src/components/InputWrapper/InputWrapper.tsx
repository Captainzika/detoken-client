import React, { FC, ReactElement, ReactNode } from 'react';
import styled, { Theme } from '~config/theme';
import ErrorIcon from '~icons/error.svg';
import { InputLabel } from '~components/InputLabel';

interface IProps {
  id?: string;
  children: ReactNode;
  error?: boolean;
  helperText?: string;
  label?: string;
  disableIcon?: boolean;
  iconPos?: 'top' | 'center' | 'bottom';
}

export const InputWrapper: FC<IProps> = (props: IProps): ReactElement => {
  const {
    error,
    helperText,
    children,
    label,
    disableIcon,
    id,
    iconPos = 'center',
  } = props;
  return (
    <Wrapper>
      <div />
      <InputLabel htmlFor={id} visible={!!label}>
        {label}
      </InputLabel>
      <Icon iconPos={iconPos}>{error && !disableIcon && <ErrorIcon />}</Icon>
      <div>{children}</div>
      <div />
      <Helper error={error}>{helperText}</Helper>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: min-content auto;
  grid-template-rows: auto auto auto;
  margin-left: -20px;
  font-size: 1rem;

  ${({ theme }): string => `
    ${theme.mediaTablet} {
    margin-left: -25px;
  `}
`;

interface IIcon {
  theme: Theme;
  iconPos: 'top' | 'center' | 'bottom';
}

const Icon = styled.div`
  align-self: center;
  text-align: center;
  height: 15px;
  width: 15px;
  margin-right: 5px;

  & > svg {
    fill: ${({ theme }): string => theme.icons.error};
  }

  ${({ theme, iconPos }: IIcon): string => `
    ${theme.mediaTablet} {
      margin-right: 5px;
      height: 20px;
      width: 20px;
    };
    
    ${iconPos === 'top' ? 'align-self: start;' : ''}
    ${iconPos === 'bottom' ? 'align-self: end;' : ''}
  `}
`;

interface IHelper {
  error?: boolean;
  theme: Theme;
}

const Helper = styled.div`
  line-height: 1rem;
  min-height: 1rem;
  margin-top: 3px;
  margin-bottom: 3px;
  color: ${({ error, theme }: IHelper): string =>
    error ? theme.input.errorBorder : theme.text.primary};
  ${({ theme }): string => `
    ${theme.mediaDesktop} {
    margin-bottom: 1px;
  `}
`;
