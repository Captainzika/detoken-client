import React, { ReactElement, ReactNode } from 'react';
import styled from '~config/theme';
import { Button, Steps } from '~components';

interface IProps {
  submitText: string;
  submitDisabled?: boolean;
  onSubmit: (e: React.FormEvent) => void;
  children: ReactNode;
  steps?: number;
  currentStep?: number;
}

export const Form = React.forwardRef<HTMLFormElement, IProps>(
  (
    {
      submitText,
      submitDisabled = false,
      onSubmit,
      children,
      steps,
      currentStep,
      ...other
    }: IProps,
    ref
  ): ReactElement => {
    return (
      // eslint-disable-next-line react/jsx-props-no-spreading
      <StyledForm onSubmit={onSubmit} ref={ref} {...other}>
        {children}
        <Button type="submit" disabled={submitDisabled}>
          {submitText}
        </Button>
        {steps && currentStep ? (
          <StepsContainer>
            <Steps steps={steps} current={currentStep} />
          </StepsContainer>
        ) : null}
      </StyledForm>
    );
  }
);

const StyledForm = styled.form`
  width: 100%;
  text-align: left;
  & > button {
    width: 100%;
    margin-top: 8px;
  }
  ${({ theme }): string => `${theme.mediaTablet} { max-width: 550px}`}

  & > div:not(:last-of-type) {
    margin-bottom: 3px;
  }
`;

const StepsContainer = styled.section`
  margin-top: 40px;
  margin-bottom: 0;
`;
