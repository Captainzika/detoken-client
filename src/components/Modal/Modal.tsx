import React, { useCallback, useEffect, FC } from 'react';
import FocusLock from 'react-focus-lock';
import { useHistory } from 'react-router-dom';
import preventScroll from 'prevent-scroll';
import CloseButtonIcon from '~icons/close.svg';
import BackSvg from '~icons/chevron.svg';
import styled, { Theme } from '~config/theme';

interface IProps {
  children: React.ReactNode;
  onClose?: () => void;
  onBack?: () => void;
  wide?: boolean;
  noClose?: boolean;
}

export const Modal: FC<IProps> = (props: IProps) => {
  const { onClose, children, onBack, wide = false, noClose } = props;
  const history = useHistory();

  const handleCloseModal = useCallback(() => {
    if (onClose) {
      onClose();
    }
  }, [onClose]);

  const handleKeydown = useCallback(
    (e: KeyboardEvent) => {
      if (['Escape', 'Esc', 27].includes(e.key)) {
        handleCloseModal();
      }
    },
    [handleCloseModal]
  );

  useEffect(() => {
    preventScroll.on();
    document.addEventListener('keydown', handleKeydown);
    const routeListenUnsubscribe = history.listen(handleCloseModal);

    return (): void => {
      preventScroll.off();
      document.removeEventListener('keydown', handleKeydown);
      routeListenUnsubscribe();
    };
  }, [handleKeydown, handleCloseModal, history]);

  return (
    <FocusLock autoFocus={false}>
      <Container>
        <Header back={!!onBack}>
          {onBack && (
            <Btn onClick={onBack} role="button">
              <BackSvg />
            </Btn>
          )}
          {noClose ? null : (
            <Btn onClick={handleCloseModal} role="button">
              <CloseButtonIcon />
            </Btn>
          )}
        </Header>
        <Content wide={wide}>{children}</Content>
        <div />
      </Container>
    </FocusLock>
  );
};

const Container = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 1000;
  overflow: auto;
  outline: 0;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  background-color: ${({ theme }): string => theme.background.modals};
`;

interface IHeader {
  theme: Theme;
  back: boolean;
}

const Header = styled.div`
  display: flex;
  justify-content: ${({ back }: IHeader): string =>
    back ? 'space-between' : 'flex-end'};
`;

const Btn = styled.div`
  cursor: pointer;
  z-index: 100;
  padding: 10px;
  transition-property: fill;
  transition-duration: 0.15s;
  transition-timing-function: ease-in;
  outline: none;
  svg {
    width: 20px;
    height: 20px;
    margin: -10px;
    padding: 10px;
    fill: ${({ theme }): string => theme.actionButton.color};
    opacity: 0.5;
    &:hover {
      opacity: 1;
    }
  }

  ${({ theme }): string => `
    ${theme.mediaTablet}{
      padding: 24px;
    };
  `}
`;

interface IContent {
  wide: boolean;
  theme: Theme;
}
const Content = styled.div`
  margin: ${({ wide }: IContent): string =>
    wide ? '-16px auto 0' : '-32px auto 0'};
  padding: ${({ wide }: IContent): string => (wide ? '40px 24px' : '44px')};
  box-sizing: border-box;
  width: ${({ wide }: IContent): string => (wide ? '448px' : '400px')};
  max-width: 100%;
  align-self: center;
  ${({ theme, wide }: IContent): string => `
    ${theme.mediaTablet}{
      margin: ${wide ? '-44px auto 0' : '-60px auto 0'}
    }
  `}
`;
