import React, { FC } from 'react';
import styled from '~config/theme';

export const FullScreenLoader: FC = () => (
  <Container>
    <img alt="loading..." />
  </Container>
);

const Container = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  width: 100%;
  height: 100%;
  z-index: 9999999;
  background-color: ${({ theme }): string => theme.background.primary};
  display: flex;
  align-items: center;
  justify-content: center;
  & > img {
    width: 50px;
    content: ${({ theme }): string => `url('/assets/${theme.loader}')`};
  }
`;
