import { IAppState } from '~dux/app/appDux';

export interface IMapState {
  userLang: IAppState['userLang'];
  isAuthorized: boolean;
  theme: boolean;
}

export interface IMapDispatch {
  switchTheme: () => void;
}
export interface IFormLayoutProps extends IMapState, IMapDispatch {}
