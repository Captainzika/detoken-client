import React from 'react';
import styled from '~config/theme';
import { Input } from '~components';

const BACKSPACE_KEY = 8;
const LEFT_ARROW_KEY = 37;
const UP_ARROW_KEY = 38;
const RIGHT_ARROW_KEY = 39;
const DOWN_ARROW_KEY = 40;
const E_KEY = 69;
const ENTER_KEY = 13;
const NOT_DIGIT_REGEXP = /[^\d]/g;
const defaultArray = ['', '', '', '', '', ''];

interface IProps {
  value?: string;
  onChange: (props: string) => void;
  autoFocus?: boolean;
  disabled?: boolean;
  onEnterKeyDown: () => void;
  error?: boolean;
}

interface StateType {
  digits: Array<string>;
}

const emptyFunc = (): void => undefined;

class PinCode2FA extends React.PureComponent<IProps, StateType> {
  textInputRefs: Array<HTMLInputElement | null>;
  // eslint-disable-next-line react/static-property-placement
  static defaultProps: Partial<IProps> = {
    autoFocus: true,
    value: '',
  };

  constructor(props: IProps) {
    super(props);
    const { value } = props;
    this.state = {
      digits: value ? value.split('') : defaultArray,
    };
    this.textInputRefs = [];
  }

  render(): React.ReactElement {
    const { autoFocus, disabled, error } = this.props;
    const { digits } = this.state;

    return (
      <div>
        {digits.map((value, i) => (
          <InputWrapper
            key={
              i // eslint-disable-line react/no-array-index-key
            }
          >
            <Input
              ref={(ref): void => {
                this.textInputRefs[i] = ref;
              }}
              data-id={i}
              // eslint-disable-next-line jsx-a11y/no-autofocus
              autoFocus={!!(autoFocus && i === 0)}
              value={value}
              type="number"
              min={0}
              max={9}
              maxLength={digits.length - i}
              autoComplete="off"
              onChange={this.handleOnChange}
              onKeyDown={this.handleOnKeyDown}
              onPaste={this.handleOnPaste}
              pattern="\d*"
              disabled={disabled}
              error={error}
            />
          </InputWrapper>
        ))}
      </div>
    );
  }

  focus = (node: HTMLInputElement | null): void => {
    if (node) {
      node.focus();
    }
  };

  clearValueAndFocus = (node: HTMLInputElement | null): void => {
    if (node) {
      node.value = '';
      this.focus(node);
    }
  };

  handleOnPaste = (event: React.ClipboardEvent<HTMLInputElement>): void => {
    event.preventDefault();

    const { onChange } = this.props;
    const pastedValue = String(event.clipboardData.getData('Text')).slice(0, 6);
    const pastedValueArray = pastedValue.split('');
    const isPastedValuesValid = !pastedValueArray.some((value) =>
      Number.isNaN(Number(value))
    );

    if (!isPastedValuesValid) {
      return;
    }

    this.setState({ digits: pastedValueArray });
    onChange(pastedValue);
  };

  handleOnChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const currentId = Number(event.currentTarget.dataset.id);
    const nextId = currentId + 1;
    const currentTarget = this.textInputRefs[currentId] || {
      value: '',
    };
    const nextTarget = this.textInputRefs[nextId];
    const { onChange } = this.props;
    const { digits: stateInput } = this.state;
    const digits = stateInput.slice();

    let value = event.currentTarget.value.replace(NOT_DIGIT_REGEXP, '');
    if (value.length > 1) {
      value = value.substr(-1);
    }

    currentTarget.value = value;
    digits[currentId] = value;

    this.setState({ digits });

    if (nextTarget) {
      digits[nextId] = '';
      this.clearValueAndFocus(nextTarget);
    }

    if (onChange) {
      onChange(digits.join(''));
    }
  };

  handleOnKeyDown = (event: React.KeyboardEvent<HTMLInputElement>): void => {
    const currentId = Number(event.currentTarget.dataset.id);
    const prevId = currentId - 1;
    const currentTarget = this.textInputRefs[currentId] || {
      value: '',
    };
    const nextTarget = this.textInputRefs[currentId + 1];
    const prevTarget = this.textInputRefs[prevId];
    const { onChange, onEnterKeyDown } = this.props;
    const { digits: stateInput } = this.state;

    const handleKeyDownMap: { [key: number]: () => void } = {
      [BACKSPACE_KEY]: (): void => {
        const digits = stateInput.slice();

        if (currentTarget.value === '') {
          if (prevTarget) {
            digits[prevId] = '';
            this.clearValueAndFocus(prevTarget);
          }
        } else {
          currentTarget.value = '';
          digits[currentId] = '';
        }

        const value = digits.join('');
        this.setState({ digits });

        if (onChange) {
          onChange(value);
        }
      },
      [LEFT_ARROW_KEY]: (): void => this.focus(prevTarget),
      [RIGHT_ARROW_KEY]: (): void => this.focus(nextTarget),
      [UP_ARROW_KEY]: emptyFunc,
      [DOWN_ARROW_KEY]: emptyFunc,
      [ENTER_KEY]: (): void => onEnterKeyDown(),
      [E_KEY]: emptyFunc,
    };

    if (handleKeyDownMap[event.keyCode]) {
      event.preventDefault();
      handleKeyDownMap[event.keyCode]();
    }
  };
}

const InputWrapper = styled.div`
  display: inline-block;
  width: calc(100% / 6);
  & > div {
    margin: 1px 4px 1px 0;
  }

  & input {
    text-align: center;
    padding: 0;
    width: 100%;
  }

  // Hide controls
  /* Chrome, Safari, Edge, Opera */
  & input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  /* Firefox */
  & input[type='number'] {
    -moz-appearance: textfield;
  }
`;

export { PinCode2FA };
