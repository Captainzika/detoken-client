import { connect } from 'react-redux';
import { MobileMenu } from './MobileMenu';
import { toggleMobileMenu, switchTheme } from '~dux/app/appDux';
import { IMapDispatch, IMapState } from './types';
import { IState } from '~dux/rootDux';
import { openModal } from '~dux/modals/modalsDux';

const mapDispatchToProps: IMapDispatch = {
  switchMenuState: toggleMobileMenu,
  switchTheme,
  openModal,
};

const mapStateToProps = (state: IState): IMapState => ({
  theme: state.app.theme,
  userLang: state.app.userLang,
  isMenuOpen: state.app.isMobileMenuOpen,
});

export default connect(mapStateToProps, mapDispatchToProps)(MobileMenu);
