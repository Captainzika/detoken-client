import React, {
  FC,
  ReactElement,
  useCallback,
  useState,
  useEffect,
} from 'react';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import { NavLink } from 'react-router-dom';
import styled, { Theme } from '~config/theme';
import { Scroll, Dropdown } from '~components';
import { IMobileMenuProps } from './types';
import PowerSvg from '~icons/power.svg';
import ChartSvg from '~icons/chart.svg';
import ListSvg from '~icons/list.svg';
import GearSvg from '~icons/gear.svg';
import { LANGUAGES } from '~config/constants';
import SunSvg from '~icons/sun.svg';
import MoonSvg from '~icons/moon.svg';
import LayersSvg from '~icons/layers.svg';

export const MobileMenu: FC<IMobileMenuProps> = ({
  switchMenuState,
  isMenuOpen,
  switchTheme,
  theme,
  userLang,
  openModal,
}: IMobileMenuProps): ReactElement => {
  const { t } = useTranslation();
  const [closedByItem, setClosedByItem] = useState(false);

  useEffect(() => {
    isMenuOpen && setClosedByItem(false);
  }, [isMenuOpen]);

  const handleLangDropdownChange = useCallback(
    (value: string): void => {
      setClosedByItem(true);
      i18next.changeLanguage(value === '中文' ? 'ZH' : value);
      switchMenuState();
    },
    [switchMenuState]
  );

  const handleThemeClick = useCallback(() => {
    setClosedByItem(true);
    switchTheme();
    switchMenuState();
  }, [switchTheme, switchMenuState]);

  const handleSignoutClick = useCallback(() => {
    setClosedByItem(true);
    switchMenuState();
    openModal({ type: 'signout', data: {} });
  }, [openModal, switchMenuState]);

  const handleMenuLinkClick = useCallback(() => {
    setClosedByItem(true);
    switchMenuState();
  }, [switchMenuState]);

  return (
    <Container show={isMenuOpen} closedByItem={closedByItem}>
      <Scroll autoHideBar>
        <StyledNavLink
          onClick={handleMenuLinkClick}
          to="/dashboard"
          activeClassName="active"
        >
          <Item>
            <ChartSvg /> <span>{t('dashboard')}</span>
          </Item>
        </StyledNavLink>
        <StyledNavLink
          onClick={handleMenuLinkClick}
          to="/positions"
          activeClassName="active"
        >
          <Item>
            <LayersSvg /> <span>{t('positions')}</span>
          </Item>
        </StyledNavLink>
        <StyledNavLink
          onClick={handleMenuLinkClick}
          to="/transfers"
          activeClassName="active"
        >
          <Item>
            <ListSvg />
            <span>{t('transfers')}</span>
          </Item>
        </StyledNavLink>
        <StyledNavLink
          onClick={handleMenuLinkClick}
          to="/account"
          activeClassName="active"
        >
          <Item>
            <GearSvg />
            <span>{t('account')}</span>
          </Item>
        </StyledNavLink>
        <Item onClick={handleSignoutClick}>
          <PowerSvg />
          <span>{t('sign_out')}</span>
        </Item>
        <Switchers>
          <Dropdown
            onChange={handleLangDropdownChange}
            value={userLang === 'ZH' ? '中文' : userLang}
            items={LANGUAGES}
          />
          <ThemeSwitcher onClick={handleThemeClick}>
            {theme ? <MoonSvg /> : <SunSvg />}
          </ThemeSwitcher>
        </Switchers>
      </Scroll>
    </Container>
  );
};

interface IContainer {
  closedByItem: boolean;
  theme: Theme;
  show: boolean;
}
const Container = styled.div`
  position: fixed;
  top: 45px;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: ${({ theme }): number => theme.zIndexMobileMenu};
  display: flex;
  ${({ show, closedByItem }: IContainer): string => {
    if (show) {
      return `
      visibility: visible;
      transform: translate(0);
      transition: all 300ms ease-in;
    `;
    }
    if (closedByItem) {
      return `
      visibility: hidden;
      transform: translateY(-100%);
    `;
    }
    return `
      visibility: hidden;
      transform: translateY(-100%);
      transition: all 300ms ease-in;
    `;
  }};
  padding: 0 55px;
  flex-direction: column;
  background: ${({ theme }): string => theme.background.secondary};
  color: ${({ theme }): string => theme.menu.text};
  font-size: 18px;
  & a {
    text-decoration: none;
    &.active {
      & > div {
        & > svg {
          fill: ${({ theme }): string => theme.menu.activeColor};
        }
        & > span {
          color: ${({ theme }): string => theme.menu.activeColor};
        }
      }
    }
  }
  ${({ theme }): string => `
    ${theme.mediaTablet} {
      display: none;
    }
  `}
`;

const StyledNavLink = styled(NavLink)`
  display: block;
  border-bottom: 1px solid ${({ theme }): string => theme.border.mobileMenu};
  width: 150px;
  margin-left: 50%;
  transform: translateX(-50%);
  &:last-child {
    border-bottom: none;
  }
`;

const Item = styled.div`
  padding: 15px 0;
  text-align: center;
  & svg {
    width: 16px;
    height: 16px;
    margin-right: 10px;
    fill: ${({ theme }): string => theme.menu.icon};
    vertical-align: middle;
  }
  & > span {
    vertical-align: middle;
    color: ${({ theme }): string => theme.menu.text};
  }
`;

const Switchers = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 16px;
  & > div:not(:last-of-type) {
    margin-right: 15px;
  }
`;

const ThemeSwitcher = styled.div`
  background: ${({ theme }): string => theme.background.primary};
  border: 1px solid ${({ theme }): string => theme.dropdown.border};
  border-radius: 4px;
  width: 36px;
  display: flex;
  align-items: center;
  justify-content: center;
  & > svg {
    width: 20px;
    fill: ${({ theme }): string => theme.menu.icon};
  }
`;
