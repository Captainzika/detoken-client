import React, { FC, ReactElement, useState, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { FormLayout, Form, TextField } from '~components';
import { IError } from '~types/Error';

interface IProps {
  error: IError | null;
  resetError: () => void;
  onSubmit: (address: string) => void;
  userAddress: string;
}

export const WithdrawAddress: FC<IProps> = ({
  error,
  resetError,
  onSubmit,
  userAddress,
}: IProps): ReactElement => {
  const [address, setAddress] = useState(userAddress || '');
  const { t } = useTranslation();

  const { key, field } = error || {};

  const handleAddressChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = e.currentTarget;
      setAddress(value);
      resetError();
    },
    [resetError]
  );

  const handleFormSubmit = useCallback(
    (e: React.FormEvent) => {
      e.preventDefault();
      onSubmit(address);
    },
    [onSubmit, address]
  );

  return (
    <FormLayout
      title={t('withdraw_header')}
      subtitle={t('enter_the_recipient_wallet_address')}
    >
      <Form
        onSubmit={handleFormSubmit}
        submitText={t('continue')}
        steps={3}
        currentStep={2}
      >
        <TextField
          value={address}
          placeholder={t('address')}
          onChange={handleAddressChange}
          error={field === 'address'}
          helperText={field === 'address' && key ? t(key) : ''}
        />
      </Form>
    </FormLayout>
  );
};
