import Big from 'big.js';
import { Dispatch } from 'react';
import { batch } from 'react-redux';
import { createSendTransaction, signSendTransaction } from '~utils/wallet';

import { sendWSMessageAction } from '~dux/app/appDux';
import {
  getAddressesSelector,
  getPrivateKeysSelector,
} from '~dux/user/userDux';
import { removeUTXO } from '~dux/wallet/walletDux';
import { ERROR_KEYS, IError } from '~types/Error';
import { IState } from '~dux/rootDux';
import { updateModalData } from '~dux/modals/modalsDux';
import { addSnackbarAction } from '~dux/notifications/notificationsDux';
import { broadcastTransaction, ISignedTransaction } from './withdrawModalApi';

export interface IPrepareTransaction {
  sendAddress: string;
  amount: string;
}

type Actions = ReturnType<
  | typeof sendWSMessageAction
  | typeof removeUTXO
  | typeof updateModalData
  | typeof addSnackbarAction
>;

export const setError = (err: IError | null) => (
  dispatch: Dispatch<Actions>
): void => {
  dispatch(
    updateModalData({ type: 'withdraw', newData: { error: err || {} } })
  );
};

export const prepareTransaction = ({
  amount,
  sendAddress,
}: IPrepareTransaction) => (
  dispatch: Dispatch<Actions>,
  getState: () => IState
): ISignedTransaction | null => {
  dispatch(updateModalData({ type: 'withdraw', newData: { loading: true } }));
  const state = getState();

  const { changeAddress } = getAddressesSelector(state);
  const { UTXOs } = state.wallet;

  let signedTransaction = null;

  try {
    const { activePrivateKey, changePrivateKey } = getPrivateKeysSelector(
      state
    );
    const transaction = createSendTransaction({
      amount: Big(amount),
      sendAddress,
      changeAddress,
      UTXOs,
    });

    signedTransaction = signSendTransaction(transaction, [
      changePrivateKey,
      activePrivateKey,
    ]);
  } catch (e) {
    dispatch(
      addSnackbarAction({
        variant: 'error',
        message: ERROR_KEYS.COMMON,
      })
    );
  }
  dispatch(updateModalData({ type: 'withdraw', newData: { loading: false } }));
  return signedTransaction;
};

export const withdraw = (transaction: ISignedTransaction) => async (
  dispatch: Dispatch<Actions>
): Promise<void> => {
  dispatch(updateModalData({ type: 'withdraw', newData: { loading: true } }));
  try {
    await broadcastTransaction({ transaction });
    batch(() => {
      transaction.inputs.forEach((input) =>
        dispatch(removeUTXO(`${input.prevTxId}:${input.outputIndex}`))
      );
      dispatch(
        updateModalData({ type: 'withdraw', newData: { success: true } })
      );
    });
  } catch (e) {
    dispatch(
      addSnackbarAction({
        variant: 'error',
        message: ERROR_KEYS.COMMON,
      })
    );
  }
  dispatch(updateModalData({ type: 'withdraw', newData: { loading: false } }));
};
