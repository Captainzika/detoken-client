import { connect } from 'react-redux';
import { IState } from '~dux/rootDux';
import { Enable2FAModal } from './Enable2FAModal';
import { init2FA, enable2FA, setError } from './Enable2FAModalDux';
import { IMapState, IMapDispatch } from './types';
import { closeModal, IEnable2FAData } from '~dux/modals/modalsDux';

const mapDispatchToProps: IMapDispatch = {
  init2FA,
  enable2FA,
  setError,
  closeModal,
};

const mapStateToProps = (state: IState): IMapState => ({
  email: state.user.email,
  twoFaMode: state.user.securityFields.twoFaMode,
  data: state.modals.enable2FA.data as IEnable2FAData,
});

export default connect(mapStateToProps, mapDispatchToProps)(Enable2FAModal);
