import { IEnable2FA } from './Enable2FAModalDux';
import { IError } from '~types/Error';
import { CloseModalAction, IEnable2FAData } from '~dux/modals/modalsDux';

export interface IMapState {
  email: string;
  twoFaMode: boolean | null;
  data: IEnable2FAData;
}

export interface IMapDispatch {
  init2FA: (password: string) => void;
  enable2FA: ({ password, code }: IEnable2FA) => void;
  setError: (err: IError | null) => void;
  closeModal: (props: CloseModalAction) => void;
}

export interface IEnable2FAModalProps extends IMapState, IMapDispatch {}

export enum STEPS {
  CURRENT_PASSWORD = 'currentPassword',
  INFO = 'info',
  QR_CODE = 'qrCode',
  TWO_FA_CODE = 'twoFaCode',
  SUCCESS = 'success',
}

export const PREV_STEPS: { [key: string]: STEPS } = {
  [STEPS.INFO]: STEPS.CURRENT_PASSWORD,
  [STEPS.QR_CODE]: STEPS.INFO,
  [STEPS.TWO_FA_CODE]: STEPS.QR_CODE,
};
