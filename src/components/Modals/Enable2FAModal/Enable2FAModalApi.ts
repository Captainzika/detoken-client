import request from '~providers/helpers';

const { post } = request;

interface IEnable2FA {
  email: string;
  passwordHash: string;
  code: string;
}

export const enable2FA = ({
  email,
  passwordHash,
  code,
}: IEnable2FA): Promise<void> => {
  const body = {
    email,
    passwordHash,
    code,
  };
  return post('/auth/2fa/enable', body);
};

interface IInit2FA {
  email: string;
  passwordHash: string;
}

interface IInit2FAResponse {
  secretCode: string;
}

export const init2FA = async ({
  email,
  passwordHash,
}: IInit2FA): Promise<IInit2FAResponse> => {
  return post('/auth/2fa/init', { email, passwordHash });
};
