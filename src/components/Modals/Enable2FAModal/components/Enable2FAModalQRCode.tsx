import React, { FC, ReactElement, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import QRCode from 'qrcode.react';
import { TOTP, Secret } from 'otpauth';
import { format } from 'date-fns';
import { FormLayout, Form, Input } from '~components';
import styled from '~config/theme';

interface IProps {
  onSubmit: () => void;
  secret: string;
  email: string;
}

export const Enable2FAModalQRCode: FC<IProps> = ({
  onSubmit,
  secret,
  email,
}: IProps): ReactElement => {
  const { t } = useTranslation();

  const qrSecret = useMemo(() => {
    const timeLabel = format(new Date(), 'dd-MM-yyyy');
    return new TOTP({
      secret: Secret.fromB32(secret),
      algorithm: 'SHA1',
      issuer: 'Detoken',
      label: `${email}-${timeLabel}`,
    });
  }, [secret, email]);

  return (
    <FormLayout title={t('enable_2fa')} subtitle={t('two_fa_enable_subtitle')}>
      <Form
        onSubmit={onSubmit}
        submitText={t('next_button')}
        steps={4}
        currentStep={3}
      >
        <Center>
          <StyledQRCode value={qrSecret.toString()} level="H" includeMargin />
          <SecretWrapper>
            <Label>{t('two_fa_secret')}</Label>
            <Input value={secret} withCopy readOnly tooltip="secret_copied" />
          </SecretWrapper>
        </Center>
      </Form>
    </FormLayout>
  );
};

const Center = styled.div`
  text-align: center;
`;

const Label = styled.div`
  color: ${({ theme }): string => theme.text.header};
  font-weight: bold;
  text-align: left;
  width: 100%;
`;

const SecretWrapper = styled.div`
  margin: 30px 0 22px;
`;

const StyledQRCode = styled(QRCode)`
  margin-top: -7px;
  margin-bottom: 2px;
`;
