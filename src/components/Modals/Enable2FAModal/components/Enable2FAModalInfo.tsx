import React, { FC, ReactElement } from 'react';
import { Trans, useTranslation } from 'react-i18next';
import { FormLayout, Form } from '~components';
import styled from '~config/theme';

interface IProps {
  onSubmit: (e: React.FormEvent) => void;
}

export const Enable2FAModalInfo: FC<IProps> = ({
  onSubmit,
}: IProps): ReactElement => {
  const { t } = useTranslation();

  return (
    <FormLayout
      title={t('enable_2fa')}
      subtitle={t('two_fa_authentication_subtitle')}
    >
      <Form
        onSubmit={onSubmit}
        submitText={t('next_button')}
        steps={4}
        currentStep={2}
      >
        <Center>
          <p>
            <Trans i18nKey="we_recommend_google_authenticator">
              Detoken suggests Google Authenticator for{' '}
              <a
                href="https://apps.apple.com/us/app/google-authenticator/id388497605"
                target="_blank"
                rel="noreferrer"
              >
                iOS
              </a>{' '}
              and{' '}
              <a
                href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2"
                target="_blank"
                rel="noreferrer"
              >
                Android
              </a>{' '}
              mobile devices.
            </Trans>
          </p>
        </Center>
      </Form>
    </FormLayout>
  );
};

const Center = styled.div`
  text-align: center;
  & > p:first-of-type {
    margin-top: 0;
    margin-bottom: 20px;
  }
  & > p:nth-of-type(2) {
    margin-top: 0;
    margin-bottom: 22px;
  }
`;
