import React, { FC, ReactElement, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { FormLayout, Button, Modal } from '~components';
import { ISignoutModal } from './types';
import styled from '~config/theme';

export const SignoutModal: FC<ISignoutModal> = ({
  signout,
  closeModal,
}: ISignoutModal): ReactElement | null => {
  const { t } = useTranslation();

  const handleCancelClick = useCallback(() => {
    closeModal({ type: 'signout' });
  }, [closeModal]);
  const handleSignoutClick = useCallback(() => {
    signout();
  }, [signout]);
  return (
    <Modal onClose={handleCancelClick}>
      <FormLayout
        title={t('sign_out')}
        subtitle={t('are_you_sure_you_wish_to_sign_out')}
      >
        <Buttons>
          <Button onClick={handleCancelClick} variant="secondary">
            {t('cancel')}
          </Button>
          <Button onClick={handleSignoutClick}>{t('sign_out')}</Button>
        </Buttons>
      </FormLayout>
    </Modal>
  );
};

const Buttons = styled.div`
  & > button:first-of-type {
    margin-right: 10px;
  }
  & > button:nth-of-type(2) {
    margin-left: 10px;
  }
`;
