import { CloseModalAction } from '~dux/modals/modalsDux';

export interface IMapDispatch {
  signout: () => void;
  closeModal: (props: CloseModalAction) => void;
}

export type ISignoutModal = IMapDispatch;
