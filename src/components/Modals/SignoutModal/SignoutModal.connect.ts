import { connect } from 'react-redux';
import { signout } from '~dux/app/appDux';
import { SignoutModal } from './SignoutModal';
import { closeModal } from '~dux/modals/modalsDux';

const mapDispatchToProps = {
  signout,
  closeModal,
};

export default connect(null, mapDispatchToProps)(SignoutModal);
