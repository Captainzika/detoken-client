import { connect } from 'react-redux';
import { closeModal } from '~dux/modals/modalsDux';
import { JurisdictionModal } from './JurisdictionModal';
import { IMapDispatch } from './types';

const mapDispatchToProps: IMapDispatch = {
  closeModal,
};

export default connect(null, mapDispatchToProps)(JurisdictionModal);
