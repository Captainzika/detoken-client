import React, { FC, ReactElement } from 'react';
import Big from 'big.js';
import { useTranslation } from 'react-i18next';
import styled from '~config/theme';
import { Modal, FormLayout, Button, Premium } from '~components';
import {
  BCH_DP,
  CURRENCY_SYMBOL,
  MS_IN_BLOCK,
  POSITION,
  SATOSHIS_IN_BCH,
} from '~config/constants';
import { ILongHedgeConfirmModal } from '../types';
import { formatFiat } from '~utils/formatters';
import { getMargin } from '~utils/contract';
import {
  getTranslationKey,
  formatDateWithTranslation,
} from '~utils/translations';
import { IConfirmModalData } from '~dux/modals/modalsDux';

interface IProps {
  data: ILongHedgeConfirmModal['data'];
  onBackBtn: () => void;
  onClose: () => void;
}

export const ContractDetails: FC<IProps> = ({
  data,
  onBackBtn,
  onClose,
}: IProps): ReactElement | null => {
  const { t } = useTranslation();
  const {
    position,
    value,
    startPrice,
    startBlockHeight,
    maturityModifier,
    lowLiquidationMulti,
    contractAddress,
    lowLiquidationPrice,
    highLiquidationPrice,
    hedgeInputSats,
    longInputSats,
    premiumSatoshis,
    premium,
    fee,
  } = data as IConfirmModalData;

  const { HEDGE, LONG } = POSITION;

  const maturityInMs = maturityModifier * MS_IN_BLOCK;
  const expiresAt = Date.now() + maturityInMs;
  const startPriceUSD = Big(startPrice).div(100);
  const lowLiquidationPriceUSD = formatFiat(
    Big(lowLiquidationPrice).div(100).toFixed(2),
    { zeroFraction: true }
  );
  const highLiquidationPriceUSD = formatFiat(
    Big(highLiquidationPrice).div(100).toFixed(2),
    { zeroFraction: true }
  );

  const margin = getMargin(lowLiquidationMulti);
  const maxHedgeBCH = position === HEDGE ? Big(value) : Big(value).mul(margin);
  const maxHedgeUSD = formatFiat(maxHedgeBCH.mul(startPriceUSD).toFixed(2), {
    zeroFraction: true,
  });
  const maxLongBCH = position === LONG ? Big(value) : Big(value).div(margin);
  const maxLongUSD = formatFiat(maxLongBCH.mul(startPriceUSD).toFixed(2), {
    zeroFraction: true,
  });

  const takerInputSatoshis =
    position === HEDGE ? hedgeInputSats : longInputSats;

  const feeBCH = Big(fee)
    .div(100)
    .times(takerInputSatoshis)
    .round(0, 3)
    .div(SATOSHIS_IN_BCH);

  const feeUSD = formatFiat(feeBCH.times(startPriceUSD).toFixed(2), {
    zeroFraction: true,
    currency: CURRENCY_SYMBOL.USD,
  });

  // premium < 0 means maker pays to taker
  const premiumSign = {
    positive: premiumSatoshis < 0,
    negative: premiumSatoshis > 0,
    none: premiumSatoshis === 0,
  };

  const premiumBCH = Big(premiumSatoshis).abs().div(SATOSHIS_IN_BCH);
  const premiumUSD = premiumBCH.mul(startPriceUSD).toFixed(2);

  return (
    <Modal onClose={onClose} onBack={onBackBtn} wide>
      <FormLayout title={t('contract_details')}>
        <Data>
          <div>
            <ItemHeader>{t('starting_block_height')}</ItemHeader>
            <ItemData>{startBlockHeight}</ItemData>
          </div>
          <div>
            <ItemHeader>{t('block_duration')}</ItemHeader>
            <ItemData>
              {maturityModifier}{' '}
              {t(getTranslationKey(maturityModifier, 'block'))}
            </ItemData>
          </div>
          <div>
            <ItemHeader>{t('expires_at_estimated')}</ItemHeader>
            <ItemData>{formatDateWithTranslation(expiresAt)}</ItemData>
          </div>
          <div>
            <ItemHeader>{t('low_liquidation_price')}</ItemHeader>
            <ItemData>{lowLiquidationPriceUSD}</ItemData>
          </div>
          <div>
            <ItemHeader>{t('high_liquidation_price')}</ItemHeader>
            <ItemData>{highLiquidationPriceUSD}</ItemData>
          </div>
          <div>
            <ItemHeader>{t('hedge_value')}</ItemHeader>
            <ItemData>
              {maxHedgeUSD} = {maxHedgeBCH.toFixed(BCH_DP)} BCH
            </ItemData>
          </div>
          <div>
            <ItemHeader>{t('long_value')}</ItemHeader>
            <ItemData>
              {maxLongUSD} = {maxLongBCH.toFixed(BCH_DP)} BCH
            </ItemData>
          </div>
          <div>
            <ItemHeader>
              {t('premium')}{' '}
              {!premiumSign.none
                ? t('premium_earn_pay', {
                    type: premiumSign.positive ? t('earn') : t('pay'),
                  })
                : null}
            </ItemHeader>
            <ItemData>
              {premiumSign.negative && '-'}
              {Big(premium).abs().times(100).toFixed(2)}% ={' '}
              <Premium
                positive={premiumSign.positive && !Big(premiumUSD).eq(0)}
                negative={premiumSign.negative && !Big(premiumUSD).eq(0)}
              >
                {premiumSign.negative && !Big(premiumUSD).eq(0) && '-'}
                {formatFiat(premiumUSD, {
                  zeroFraction: true,
                  currency: CURRENCY_SYMBOL.USD,
                })}
              </Premium>
              {' = '}
              {premiumSign.negative &&
                !Big(premiumBCH.toFixed(BCH_DP)).eq(0) &&
                '-'}
              {premiumBCH.toFixed(BCH_DP)} BCH
            </ItemData>
          </div>
          <div>
            <ItemHeader>{t('detoken_fee')}</ItemHeader>
            <ItemData>
              {fee}% = {feeUSD} = {feeBCH.toFixed(BCH_DP)} BCH
            </ItemData>
          </div>
          <div>
            <ItemHeader>{t('contract_address')}</ItemHeader>
            <ItemData>{contractAddress}</ItemData>
          </div>
        </Data>
        <StyledButton fullWidth variant="secondary" onClick={onBackBtn}>
          {t('back_button')}
        </StyledButton>
      </FormLayout>
    </Modal>
  );
};

const Data = styled.div`
  text-align: left;
  margin-bottom: 30px;
  width: 100%;
  box-sizing: border-box;
  border-radius: 4px;
  line-height: 25px;
  border: 1px solid ${({ theme }): string => theme.border.default};
  background: ${({ theme }): string => theme.background.tertiary};
  padding: 20px;
  & > div:not(:last-of-type) {
    margin-bottom: 15px;
  }
`;

const ItemHeader = styled.div`
  font-weight: bold;
  color: ${({ theme }): string => theme.text.header};
`;

const ItemData = styled.span`
  color: ${({ theme }): string => theme.text.primary};
  overflow-wrap: break-word;
`;

const StyledButton = styled(Button)`
  width: calc(100% - 8px);
`;
