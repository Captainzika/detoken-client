import { connect } from 'react-redux';
import { Disable2FAModal } from './Disable2FAModal';
import { closeModal, IDisable2FAData } from '~dux/modals/modalsDux';
import { IState } from '~dux/rootDux';
import { setError, disable2FA } from './Disable2FAModalDux';
import { IMapState } from './types';

const mapDispatchToProps = {
  closeModal,
  setError,
  disable2FA,
};

const mapStateToProps = (state: IState): IMapState => ({
  data: state.modals.disable2FA.data as IDisable2FAData,
});

export default connect(mapStateToProps, mapDispatchToProps)(Disable2FAModal);
