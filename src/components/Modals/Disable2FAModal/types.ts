import { IError } from '~types/Error';
import { CloseModalAction, IDisable2FAData } from '~dux/modals/modalsDux';
import { IDisable2FA } from './Disable2FAModalDux';

export interface IMapState {
  data: IDisable2FAData;
}

export interface IMapDispatch {
  setError: (err: IError | null) => void;
  closeModal: (props: CloseModalAction) => void;
  disable2FA: ({ password, code }: IDisable2FA) => void;
}

export interface IDisable2FAModalProps extends IMapState, IMapDispatch {}
