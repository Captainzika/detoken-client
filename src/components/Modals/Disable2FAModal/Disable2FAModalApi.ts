import request from '~providers/helpers';

const { post } = request;

export interface IDisable2FA {
  email: string;
  passwordHash: string;
  code: string;
}

export const disable2FA = async ({
  email,
  passwordHash,
  code,
}: IDisable2FA): Promise<void> => {
  const body = {
    email,
    passwordHash,
    code,
  };
  return post('/auth/2fa/disable', body);
};
