import React, { FC, useCallback, ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import QRCode from 'qrcode.react';
import { Modal, Button, FormLayout, Input } from '~components';
import styled from '~config/theme';
import { IDepositModalProps } from './types';

export const DepositModal: FC<IDepositModalProps> = ({
  theme,
  closeModal,
  addresses: { activeAddress },
}: IDepositModalProps): ReactElement | null => {
  const { t } = useTranslation();

  const handleCloseModal = useCallback(() => {
    closeModal({ type: 'deposit' });
  }, [closeModal]);

  return (
    <Modal onClose={handleCloseModal}>
      <FormLayout title={t('receive')} subtitle={t('scan_qr_code_subtitle')}>
        <QRCodeWrapper>
          <QRCode
            value={activeAddress}
            includeMargin={!theme}
            level="H"
            size={145}
          />
        </QRCodeWrapper>
        <Subtitle>{t('send_bch_to_your_receiving_address')}</Subtitle>

        <Label>{t('wallet_address')}</Label>
        <InputWrapper>
          <Input
            defaultValue={activeAddress}
            withCopy
            fullWidth
            tooltip="copied"
            readOnly
          />
        </InputWrapper>
        <Button onClick={handleCloseModal} fullWidth variant="secondary">
          {t('close_button')}
        </Button>
      </FormLayout>
    </Modal>
  );
};

const Subtitle = styled.p`
  color: ${({ theme }): string => theme.text.primary};
  margin: 0 0 30px 0;
  line-height: 1.125rem;
  ${({ theme }): string => `
    ${theme.mediaDesktop}{
      line-height: calc(7rem / 6);
    };
  `};
`;

const QRCodeWrapper = styled.div`
  margin-bottom: 30px;
  display: inherit;
`;

const Label = styled.div`
  color: ${({ theme }): string => theme.text.header};
  font-weight: bold;
  text-align: left;
  width: 100%;
`;

const InputWrapper = styled.div`
  margin-bottom: 30px;
  width: 100%;
`;
