import { connect } from 'react-redux';
import { IState } from '~dux/rootDux';
import { DepositModal } from './DepositModal';
import { IMapState, IMapDispatch } from './types';
import { closeModal } from '~dux/modals/modalsDux';
import { getAddressesSelector } from '~dux/user/userDux';

const mapStateToProps = (state: IState): IMapState => ({
  data: state.modals.deposit.data,
  addresses: getAddressesSelector(state),
  theme: state.app.theme,
});

const mapDispatchToProps: IMapDispatch = {
  closeModal,
};

export default connect(mapStateToProps, mapDispatchToProps)(DepositModal);
