import { connect } from 'react-redux';
import { PositionModal } from './PositionModal';
import { closeModal, IPositionModalData } from '~dux/modals/modalsDux';
import { IState } from '~dux/rootDux';
import { IMapState, IMapDispatch } from './types';

const mapDispatchToProps: IMapDispatch = {
  closeModal,
};

const mapStateToProps = (state: IState): IMapState => ({
  blockHeight: state.app.oraclePrice.blockHeight,
  oraclePrice: state.app.oraclePrice.price,
  data: state.modals.position.data as IPositionModalData,
});

export default connect(mapStateToProps, mapDispatchToProps)(PositionModal);
