import React, { FC, ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import Big from 'big.js';
import styled from '~config/theme';
import { Modal, FormLayout, Button, Premium } from '~components';
import { IPositionModal } from '../types';
import { IContractData } from '~utils/contract';
import { EXPLORER_URL } from '~config/constants';
import {
  getTranslationKey,
  formatDateWithTranslation,
} from '~utils/translations';
import { formatFiat } from '~utils/formatters';

interface IProps {
  data: IPositionModal['data'];
  onBackBtn: () => void;
  onClose: () => void;
  contract: IContractData;
}

export const PositionDetails: FC<IProps> = ({
  onBackBtn,
  onClose,
  contract,
}: IProps): ReactElement | null => {
  const { t } = useTranslation();

  const {
    startBlockHeight,
    blockDuration,
    createdAt,
    expiresAt,
    lowLiquidationPrice,
    highLiquidationPrice,
    contractAddress,
    premium,
    premiumUSD,
    premiumBCH,
    fee,
    feeBCH,
    feeUSD,
    totalCostBCH,
    totalCostUSD,
    startHedgeValBCH,
    startHedgeValUSD,
    startLongValBCH,
    startLongValUSD,
    oraclePubKey,
  } = contract;

  const premiumSign = {
    positive: Big(premium).lt(0),
    negative: Big(premium).gt(0),
    none: Big(premium).eq(0),
  };

  return (
    <Modal onClose={onClose} onBack={onBackBtn} wide>
      <FormLayout title={t('contract_details')}>
        <Data>
          <FirstColumn>
            <div>
              <ItemHeader>{t('starting_block_height')}</ItemHeader>
              <ItemData>{startBlockHeight}</ItemData>
            </div>
            <div>
              <ItemHeader>{t('block_duration')}</ItemHeader>
              <ItemData>
                {blockDuration} {t(getTranslationKey(blockDuration, 'block'))}
              </ItemData>
            </div>
            <div>
              <ItemHeader>{t('created_at')}</ItemHeader>
              <ItemData>{formatDateWithTranslation(createdAt)}</ItemData>
            </div>
            <div>
              <ItemHeader>{t('expires_at_estimated')}</ItemHeader>
              <ItemData>{formatDateWithTranslation(expiresAt)}</ItemData>
            </div>
            <div>
              <ItemHeader>{t('low_liquidation_price')}</ItemHeader>
              <ItemData>{lowLiquidationPrice}</ItemData>
            </div>
            <div>
              <ItemHeader>{t('high_liquidation_price')}</ItemHeader>
              <ItemData>{highLiquidationPrice}</ItemData>
            </div>
            <div>
              <ItemHeader>{t('contract_address')}</ItemHeader>
              <ItemData>{contractAddress}</ItemData>
            </div>
          </FirstColumn>
          <SecondColumn>
            <div>
              <ItemHeader>
                {t('premium')}{' '}
                {Big(premiumBCH).abs().gt(0)
                  ? t('premium_earn_pay', {
                      type: premiumSign.positive ? t('earn') : t('pay'),
                    })
                  : null}
              </ItemHeader>
              <ItemData>
                {premiumSign.negative && '-'}
                {Big(premium).abs().times(100).toFixed(2)}% ={' '}
                <Premium
                  positive={premiumSign.positive && !Big(premiumUSD).eq(0)}
                  negative={premiumSign.negative && !Big(premiumUSD).eq(0)}
                >
                  {premiumSign.negative && !Big(premiumUSD).eq(0) && '-'}
                  {formatFiat(premiumUSD, {
                    zeroFraction: true,
                  })}
                </Premium>
                {' = '}
                {premiumSign.negative && !Big(premiumUSD).eq(0) && '-'}
                {premiumBCH} BCH
              </ItemData>
            </div>
            <div>
              <ItemHeader>{t('detoken_fee')}</ItemHeader>
              <ItemData>
                {fee}% = {feeUSD} = {feeBCH} BCH
              </ItemData>
            </div>
            <div>
              <ItemHeader>{t('total_contract_cost')}</ItemHeader>
              <ItemData>
                {totalCostUSD} = {totalCostBCH} BCH
              </ItemData>
            </div>
            <div>
              <ItemHeader>{t('start_hedge_value')}</ItemHeader>
              <ItemData>
                {startHedgeValUSD} = {startHedgeValBCH} BCH
              </ItemData>
            </div>
            <div>
              <ItemHeader>{t('start_long_value')}</ItemHeader>
              <ItemData>
                {startLongValUSD} = {startLongValBCH} BCH
              </ItemData>
            </div>
            <div>
              <ItemHeader>{t('oracle_public_key')}</ItemHeader>
              <ItemData>{oraclePubKey}</ItemData>
            </div>
          </SecondColumn>
        </Data>
        <Explorer>
          <a
            target="_blank"
            rel="noreferrer"
            href={`${EXPLORER_URL}/bch/address/${contractAddress}`}
          >
            {t('block_explorer_link')}
          </a>
        </Explorer>
        <StyledButton fullWidth variant="secondary" onClick={onBackBtn}>
          {t('back_button')}
        </StyledButton>
      </FormLayout>
    </Modal>
  );
};

const Data = styled.div`
  text-align: left;
  margin-bottom: 15px;
  width: 100%;
  box-sizing: border-box;
  border-radius: 4px;
  line-height: 25px;
  border: 1px solid ${({ theme }): string => theme.border.default};
  background: ${({ theme }): string => theme.background.tertiary};
  padding: 20px;

  ${({ theme }): string => `
    ${theme.mediaDesktop}{
     width: 660px;
     display: flex;
     }
    }
  `}
`;

const ItemHeader = styled.div`
  font-weight: bold;
  color: ${({ theme }): string => theme.text.header};
  line-height: 21px;
`;

const ItemData = styled.span`
  color: ${({ theme }): string => theme.text.primary};
  overflow-wrap: break-word;
`;

const StyledButton = styled(Button)`
  width: calc(100% - 8px);
`;

const Explorer = styled.div`
  text-align: center;
  margin-bottom: 30px;
  font-size: 14px;
  ${({ theme }): string => `
    ${theme.mediaDesktop}{
      text-align: left;
    }
  `}
`;
const FirstColumn = styled.div`
  display: flex;
  flex-direction: column;
  & > div {
    margin-bottom: 15px;
  }
  ${({ theme }): string => `
    ${theme.mediaDesktop}{
      & > div:last-of-type {
        margin-bottom: 0;
      }    
      max-width: 300px;
      margin-right: 8px;
    }
  `}
`;

const SecondColumn = styled.div`
  display: flex;
  flex-direction: column;
  & > div:not(:last-of-type) {
    margin-bottom: 15px;
  }
  ${({ theme }): string => `
    ${theme.mediaDesktop}{
      max-width: 300px;
      margin-left: 8px;
    }
  `}
`;
