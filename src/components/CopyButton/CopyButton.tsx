import React, { FC, ReactElement, useCallback } from 'react';
import copyToClipboard from 'copy-to-clipboard';
import { useTranslation } from 'react-i18next';
import CopySvg from '~icons/copy.svg';
import styled from '~config/theme';
import { Tooltip } from '~components';

interface IProps {
  value: string | null;
}

export const CopyButton: FC<IProps> = ({ value }: IProps): ReactElement => {
  const { t } = useTranslation();

  const handleClickCopy = useCallback(() => {
    value && copyToClipboard(value);
  }, [value]);

  return (
    <Tooltip message={t('copied')} trigger="click">
      <CopyIcon onClick={handleClickCopy} />
    </Tooltip>
  );
};

const CopyIcon = styled<any>(CopySvg)`
  width: 16px;
  height: 16px;
  cursor: pointer;
  padding: 0 6px;
  vertical-align: -1px;
  fill: ${({ theme }): string => theme.text.primary};
  &:hover {
    fill: ${({ theme }): string => theme.link.hover};
  }
`;
