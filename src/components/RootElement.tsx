import React, { FunctionComponent } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from '~config/store';
import '~config/i18n';
import { RootRouter } from './RootRouter';

export const RootElement: FunctionComponent = () => {
  return (
    <BrowserRouter>
      <Provider store={store}>
        <RootRouter />
      </Provider>
    </BrowserRouter>
  );
};
