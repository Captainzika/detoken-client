import { connect } from 'react-redux';
import { Header } from './Header';
import { openModal } from '~dux/modals/modalsDux';

const mapDispatchToProps = {
  openModal,
};

export default connect(null, mapDispatchToProps)(Header);
