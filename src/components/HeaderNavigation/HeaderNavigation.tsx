import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { NavLink } from 'react-router-dom';
import BurgerSvg from '~icons/burger.svg';
import styled, { Theme } from '~config/theme';
import { IHeaderNavigationProps } from './types';

interface IMainMenuItem {
  name: string;
  path: string;
}

export const MainMenuItems: Array<IMainMenuItem> = [
  {
    name: 'dashboard',
    path: '/dashboard',
  },
  {
    name: 'positions',
    path: '/positions',
  },
  {
    name: 'transfers',
    path: '/transfers',
  },
  {
    name: 'account',
    path: '/account',
  },
];

export const HeaderNavigation: FC<IHeaderNavigationProps> = (
  props: IHeaderNavigationProps
) => {
  const { mobileMenuClick, isMobileMenuOpen } = props;
  const { t } = useTranslation();

  const handleMobileMenu = (): void => {
    mobileMenuClick();
  };

  return (
    <Container>
      <MainMenu>
        {MainMenuItems.map(({ name, path }: IMainMenuItem) => {
          return (
            <HeaderLink key={name}>
              <NavLink to={path} key={path} activeClassName="active">
                {t(name)}
              </NavLink>
            </HeaderLink>
          );
        })}
      </MainMenu>

      <MobileMenu onClick={handleMobileMenu} onKeyPress={handleMobileMenu}>
        <MobileMenuBtn active={isMobileMenuOpen}>
          <BurgerSvg />
        </MobileMenuBtn>
      </MobileMenu>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  ${({ theme }): string => `
    ${theme.mediaTablet}{
      position: absolute;
      height: 100%;
      top: 0;
      left: 50%;
      transform: translateX(-50%);
    }
  `}
`;

const MainMenu = styled.div`
  align-items: center;
  display: none;

  & > div:not(:last-child) {
    margin-right: 25px;
  }

  ${({ theme }): string => `
    ${theme.mediaTablet}
    {
      display: flex;
    }
    ${theme.mediaDesktop}
    {
      & > div:not(:last-child) {
        margin-right: 40px;
      }
    }
  `}
`;

const MobileMenu = styled.div(
  ({ theme }) => `
  height: 17px;
    
  ${theme.mediaTablet}
  {
    display: none;
  }
`
);

interface IMobileMenuBtn {
  theme: Theme;
  active: boolean;
}

const MobileMenuBtn = styled.svg`
  fill: ${({ theme, active }: IMobileMenuBtn): string =>
    active ? theme.menu.activeColor : theme.primaryAppColor};
  width: 25px;
  height: 30px;
  margin: -7px;
  padding: 7px;
`;

const HeaderLink = styled.div`
  font-size: 1rem;
  & > a {
    padding-bottom: 1px;
    user-select: none;
    color: ${({ theme }): string => theme.menu.text};
    &.active {
      border-bottom: 3px solid ${({ theme }): string => theme.menu.activeColor};
      padding-bottom: 2px;
      color: ${({ theme }): string => theme.primaryAppColor};
    }
    &:hover {
      text-decoration: none;
      color: ${({ theme }): string => theme.menu.hover};
    }
  }
`;
