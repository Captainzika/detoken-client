import { connect } from 'react-redux';
import { openModal } from '~dux/modals/modalsDux';
import { SendReceiveSection } from './SendReceiveSection';

const mapDispatchToProps = {
  openModal,
};

export default connect(null, mapDispatchToProps)(SendReceiveSection);
