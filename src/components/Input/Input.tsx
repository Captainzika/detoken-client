import React, { ReactElement, ReactNode, useCallback } from 'react';
import copyToClipboard from 'copy-to-clipboard';
import { useTranslation } from 'react-i18next';
import styled, { Theme } from '~config/theme';
import CopySvg from '~icons/copy.svg';
import { Tooltip } from '~components/Tooltip';

export interface IInputProps extends React.InputHTMLAttributes<any> {
  error?: boolean;
  btnText?: string;
  btnClick?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  withCopy?: boolean;
  disableBtn?: boolean;
  tooltip?: string;
  fullWidth?: boolean;
  rightComponent?: ReactNode;
}

export const Input = React.forwardRef<HTMLInputElement, IInputProps>(
  (props: IInputProps, ref): ReactElement => {
    const {
      error = false,
      btnText,
      btnClick,
      disabled,
      value,
      withCopy,
      defaultValue,
      disableBtn,
      tooltip = 'copied',
      fullWidth = false,
      rightComponent,
      ...rest
    } = props;

    const { t } = useTranslation();

    const handleCopyToClipboard = useCallback(() => {
      if (disabled) {
        return;
      }
      const copyValue = String(value || defaultValue || '');
      copyToClipboard(copyValue);
    }, [value, disabled, defaultValue]);

    return (
      <Wrapper fullWidth={fullWidth}>
        <StyledInput
          // eslint-disable-next-line react/jsx-props-no-spreading
          {...rest}
          error={error}
          button={!!btnText || !!withCopy}
          ref={ref}
          disabled={disabled}
          value={value}
          defaultValue={defaultValue}
        />
        {rightComponent && rightComponent}
        {btnText && (
          <InputButton onClick={btnClick} type="button" disabled={!!disableBtn}>
            {btnText}
          </InputButton>
        )}
        {withCopy && (
          <Tooltip message={t(tooltip)} trigger="click">
            <InputButton
              onClick={handleCopyToClipboard}
              type="button"
              disabled={disableBtn || disabled}
            >
              <CopyIcon>
                <CopySvg />
              </CopyIcon>
            </InputButton>
          </Tooltip>
        )}
      </Wrapper>
    );
  }
);

interface IProps {
  theme: any;
  error: boolean;
  button?: boolean;
  copy?: boolean;
}

interface IWrapper {
  theme: Theme;
  fullWidth: boolean;
}
const Wrapper = styled.div`
  height: 38px;
  display: flex;
  width: ${({ fullWidth }: IWrapper): string => (fullWidth ? '100%' : 'auto')};
  position: relative;
`;

const StyledInput = styled.input<IProps>(
  ({ theme, error = false, button }) => `
  background: ${theme.input.background};
  border: 2px solid ${
    error ? theme.input.errorBorder : theme.input.borderColor
  };
  box-sizing: border-box;
  border-radius: ${button ? '3px 0px 0px 3px;' : '3px;'}
  color: ${theme.text.primary};
  caret-color: ${theme.text.primary};
  outline: none;
  flex-grow: 1;
  min-width: 0;
  height: 100%;
  width: 100%;
  font-size: 1rem;
  padding: 0 12px;
  font-family: 'Inter', serif;
  -webkit-appearance: none;
  
  // for firefox
  filter: none;
  // for chrome
  &:-webkit-autofill{
   -webkit-text-fill-color: ${theme.text.primary} !important;
   -webkit-box-shadow: 0 0 0px 1000px ${theme.input.background} inset;
  }
  // safari
  &::-webkit-credentials-auto-fill-button {
    background-color: ${theme.text.primary};
  }
  
  &:focus {
    border-color: ${
      error ? theme.input.focusErrorBorder : theme.input.focusBorder
    };
  }
  
  &:disabled {
    border-color: ${theme.input.disabledBorder};
    color: ${theme.input.disabledText};
  }
  
  &::placeholder {
    color: ${theme.input.placeholder} 
  }
 
`
);

export const InputButton = styled.button`
  cursor: pointer;
  height: 100%;
  padding: 0 18px;
  background-color: ${({ theme }): string => theme.secondaryButton.color};
  border: none;
  outline: none;
  border-radius: 0 3px 3px 0;
  margin: 0;

  transition: background-color 0.2s ease-in-out;
  color: ${({ theme }): string => theme.secondaryButton.text};
  font-size: 1rem;
  white-space: nowrap;
  &:hover {
    background-color: ${({ theme }): string =>
      theme.secondaryButton.hoverColor};
  }

  &:disabled {
    background-color: ${({ theme }): string =>
      theme.secondaryButton.disabledColor};
    cursor: unset;
  }
`;

const CopyIcon = styled.div`
  height: 16px;
  & svg {
    height: 16px;
    fill: ${({ theme }): string => theme.secondaryButton.text};
  }
`;
