import { connect } from 'react-redux';
import { IState } from '~dux/rootDux';
import { IMapDispatch, IMapState } from './types';
import { switchTheme } from '~dux/app/appDux';
import { TopMenu } from './TopMenu';
import { openModal } from '~dux/modals/modalsDux';
import { isActivePositionSelector } from '~pages/PositionsPage/positionsPageDux';

const mapDispatchToProps: IMapDispatch = {
  switchTheme,
  openModal,
};

const mapStateToProps = (state: IState): IMapState => ({
  theme: state.app.theme,
  userLang: state.app.userLang,
  isActivePosition: isActivePositionSelector(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(TopMenu);
