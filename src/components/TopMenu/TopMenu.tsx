import React, { FC, ReactElement, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import styled from '~config/theme';
import { LANGUAGES } from '~config/constants';
import { Dropdown } from '~components/Dropdown';
import { ITopMenuProps } from './types';
import MoonSvg from '~icons/moon.svg';
import SunSvg from '~icons/sun.svg';
import PowerSvg from '~icons/power.svg';
import { UserBalance, OraclePrice } from '~components';
import { ContractBalances } from '~pages/DashboardPage/components/ContractBalances';

export const TopMenu: FC<ITopMenuProps> = ({
  theme,
  switchTheme,
  userLang,
  openModal,
  isActivePosition,
}: ITopMenuProps): ReactElement => {
  const { t } = useTranslation();

  const handleLangDropdownChange = useCallback((value: string): void => {
    i18next.changeLanguage(value === '中文' ? 'ZH' : value);
  }, []);

  const handleSignoutClick = useCallback(() => {
    openModal({ type: 'signout', data: {} });
  }, [openModal]);

  return (
    <Container>
      <Stats>
        <OraclePrice />
        <UserBalance />
        {isActivePosition && (
          <ContractBalancesWrapper>
            <ContractBalances />
          </ContractBalancesWrapper>
        )}
      </Stats>
      <Switchers>
        <StyledDropdown
          items={LANGUAGES}
          onChange={handleLangDropdownChange}
          value={userLang === 'ZH' ? '中文' : userLang}
          compact
          minWidth={30}
        />
        <ThemeIcon
          title={t(theme ? 'dark_theme' : 'light_theme')}
          onClick={switchTheme}
        >
          {theme ? <MoonSvg /> : <SunSvg />}
        </ThemeIcon>
        <PowerIcon title={t('sign_out')} onClick={handleSignoutClick}>
          <PowerSvg />
        </PowerIcon>
      </Switchers>
    </Container>
  );
};

const Container = styled.div`
  box-sizing: border-box;
  min-height: 32px;
  height: 32px;
  justify-content: space-between;
  width: 100%;
  border-bottom: 1px solid ${({ theme }): string => theme.border.default};
  display: none;
  ${({ theme }): string => `
    ${theme.mediaTablet}{
      display: flex;
      padding: 6px 15px; 
    }
    ${theme.mediaDesktop}{
      padding: 6px 60px;  
    }
  `};

  & > div {
    align-self: center;
  }
`;

const ThemeIcon = styled.div`
  align-self: center;
  cursor: pointer;
  margin-right: 25px;
  display: flex;

  & svg {
    fill: ${({ theme }): string => theme.menu.icon};
    width: 16px;
    &:hover {
      fill: ${({ theme }): string => theme.menu.hover};
    }
  }
`;

const StyledDropdown = styled(Dropdown)`
  font-size: 14px;
`;

const PowerIcon = styled.div`
  cursor: pointer;
  height: 16px;
  width: 16px;
  display: flex;
  & > svg {
    width: 16px;
    fill: ${({ theme }): string => theme.menu.icon};
    &:hover {
      fill: ${({ theme }): string => theme.menu.hover};
    }
  }
`;

const Switchers = styled.div`
  display: flex;
  & > div {
    align-self: center;
    &:first-of-type {
      margin-right: 17px;
    }
    &:nth-of-type(2) {
      margin-right: 21px;
    }
  }
`;

const Stats = styled.div`
  display: flex;
`;

const ContractBalancesWrapper = styled.div`
  display: none;
  & > div {
    margin-left: 25px;
  }
  ${({ theme }): string => `
    ${theme.mediaDesktop}{
      display: flex;
    }
  `}
`;
