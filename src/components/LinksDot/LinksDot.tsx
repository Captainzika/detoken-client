import styled from '~config/theme';

export const LinksDot = styled.div`
  width: 3px;
  height: 3px;
  margin: 0 5px;
  border-radius: 50%;
  background-color: ${({ theme }): string => theme.dot};
  align-self: center;
`;
