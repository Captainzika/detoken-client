import React, {
  FC,
  ReactElement,
  useState,
  useCallback,
  ReactNode,
  useEffect,
} from 'react';
import styled, { Theme } from '~config/theme';

type TriggerType = 'hover' | 'click';

interface ITooltip {
  message: string;
  trigger?: TriggerType;
  children: ReactNode;
  position?: 'top' | 'bottom';
}

const TOOLTIP_TIMEOUT = 2500; // 2.5 seconds;

export const Tooltip: FC<ITooltip> = ({
  message,
  trigger = 'hover',
  children,
  position = 'top',
}: ITooltip): ReactElement => {
  const [show, setShow] = useState(false);
  const [timeoutId, setTimeoutId] = useState<number>(0);

  useEffect(() => {
    return (): void => clearTimeout(timeoutId);
  }, [timeoutId]);

  const hideTooltip = useCallback(() => {
    setShow(false);
  }, []);

  const showTooltip = useCallback(() => setShow(true), []);

  const hoverOnTooltip = useCallback(() => {
    trigger === 'hover' && showTooltip();
  }, [trigger, showTooltip]);

  const hoverOffTooltip = useCallback(() => {
    trigger === 'hover' && hideTooltip();
  }, [trigger, hideTooltip]);

  const handleClick = useCallback(() => {
    showTooltip();
    const id = window.setTimeout(hideTooltip, TOOLTIP_TIMEOUT);
    setTimeoutId(id);
  }, [showTooltip, hideTooltip]);

  return (
    <Body onMouseLeave={hoverOffTooltip}>
      {show && (
        <MessageContainer position={position}>
          <Message>{message}</Message>
        </MessageContainer>
      )}
      <ChildWrapper
        onMouseOver={hoverOnTooltip}
        onFocus={hoverOnTooltip}
        onClick={handleClick}
      >
        {children}
      </ChildWrapper>
    </Body>
  );
};

const Body = styled.span`
  position: relative;
  display: inline-block;
`;

interface IMessageContainer {
  position: string;
  theme: Theme;
}

const MessageContainer = styled.div(
  ({ theme, position }: IMessageContainer) => `
  position: absolute;
  z-index: 10;
  bottom: ${position === 'top' ? '97%' : '-217%'};
  left: 50%;
  padding-bottom: 9px;
  transform: translateX(-50%);
  ${theme.mediaTablet}{
    bottom: ${position === 'top' ? '97%' : '-197%'};
  }


  &::${position === 'top' ? 'after' : 'before'} {
    content: '';
    position: absolute;
    border-width: 10px;
    border-style: solid;
    border-color: ${
      position === 'top'
        ? `${theme.secondaryButton.color} transparent transparent transparent`
        : `transparent transparent ${theme.secondaryButton.color} transparent`
    };
    bottom: ${position === 'top' ? '-8px' : '95%'};
    left: 50%;
    transform: translateX(-50%);
    pointer-events: none;
  }
  `
);

const Message = styled.div`
  background-color: ${({ theme }): string => theme.secondaryButton.color};
  border-radius: 3px;
  color: ${({ theme }): string => theme.secondaryButton.text};
  font-size: 14px;
  padding: 4px 10px;
  white-space: nowrap;
`;

const ChildWrapper = styled.span`
  height: 100%;
  width: 100%;
`;
