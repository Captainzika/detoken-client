import styled, { CreateStyled } from '@emotion/styled';
import { light } from './light';
import { dark } from './dark';

export { light, dark };

export type Theme = typeof dark;

export default styled as CreateStyled<Theme>;
