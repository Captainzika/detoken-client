import { commonParams } from './common';

export const dark = {
  ...commonParams,
  primaryAppColor: '#FFFFFF',
  scrollBarColor: '#1B1B1B',
  shadowColor: 'rgba(0, 0, 0, 0)',
  loader: 'loaderDark.svg',
  dot: '#9095A7',
  text: {
    primary: '#FFFFFF',
    secondary: '#D1D1D1',
    error: '#C24D5B',
    header: '#D1D1D1',
    fiat: '#AAB0C7',
  },
  background: {
    unauthorized: '#1C1C1C',
    primary: '#2F3033',
    secondary: '#1B1B1B',
    tertiary: '#1B1B1B',
    error: '#A23F4B',
    success: '#17864B',
    topHeader: '#2F3033',
    mobileMenu: '#1c1c1c',
    modals: '#2f3033',
    stepDone: '#7078bd',
    stepGrayed: '#9095A7',
  },
  link: {
    hover: '#FFFFFF',
  },
  border: {
    default: '#1b1b1b',
    error: '#C24D5B',
    copySeedWords: '#7178BD',
    accountSection: '#1B1B1B',
    section: '#2F3033',
    mobileMenu: '#D1D1D1',
  },
  button: {
    color: '#7178BD',
    text: '#FFFFFF',
    hoverColor: '#9096D9', //  background color on hover
    disabledColor: '#3E4046',
    textHover: 'red',
    activeColor: 'green',
  },
  secondaryButton: {
    color: '#9095A7',
    text: '#FFFFFF',
    hoverColor: '#B0B6C9', // background color on hover
    disabledColor: '#3E4046',
  },
  linkButton: {
    color: 'transparent',
    text: '#FFFFFF',
    hoverColor: '#FFFFFF', // background color on hover
    disabledColor: '#f0f0f1',
  },
  actionButton: {
    // back/close buttons on modal
    color: '#d1d1d1',
  },
  input: {
    background: '#1B1B1B',
    borderColor: '#9095A7',
    errorBorder: '#C24D5B',
    focusBorder: '#FFFFFF',
    focusErrorBorder: '#A13F4B',
    disabledBorder: '#f0f0f1',
    disabledText: '#d4d7e3',
    placeholder: '#9095A7',
  },
  snackbar: {
    success: {
      border: '#9EE1BD',
      background: '#17864B',
      text: '#9EE1BD',
    },
    info: {
      border: '#A9D9E9',
      background: '#006D8F',
      text: '#A9D9E9',
    },
    error: {
      border: '#EFD1D5',
      background: '#A23F4B',
      text: '#EFD1D5',
    },
  },
  dropdown: {
    border: '#D1D1D1',
    text: '#D1D1D1',
    textHover: '#FFFFFF',
  },
  menu: {
    text: '#D1D1D1',
    activeColor: '#7178BD',
    hover: '#FFFFFF',
    icon: '#D1D1D1',
  },
  footer: {
    text: '#D1D1D1',
    hover: '#FFFFFF',
  },
  icons: {
    error: '#C24D5B',
    success: '#17864B',
    fail: '#A13F4B',
  },
  logo: {
    primaryColor: '#FFFFFF',
    secondaryColor: '#7178BD',
    dotColor: '#FFFFFF',
  },
};
