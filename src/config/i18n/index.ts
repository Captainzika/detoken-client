import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';
import storage from 'local-storage-fallback';
import store from '~config/store';
import { setUserLangAction } from '~dux/app/appDux';

import { RU } from './RU';
import { EN } from './EN';
import { ZH } from './ZH';
import { LANGUAGES, STORAGE_KEYS } from '~config/constants';

export const availableLanguages = ['EN', 'RU', 'ZH'];

const resources = {
  EN: { translation: EN },
  RU: { translation: RU },
  ZH: { translation: ZH },
};

i18next.use(initReactI18next).init(
  {
    resources,
    fallbackLng: 'EN',
    lng: storage.getItem(STORAGE_KEYS.USER_LANGUAGE) || LANGUAGES[0],
    interpolation: {
      escapeValue: false,
      format: (value, format): string => {
        if (value && format === 'lowercase') return value.toLowerCase();
        if (value && format === 'capitalize')
          return value[0].toUpperCase() + value.slice(1);
        return value;
      },
    }, // React already does escaping
    whitelist: availableLanguages,
    initImmediate: false,
  },
  // eslint-disable-next-line consistent-return
  (err) => {
    if (err) return console.log('something went wrong', err);
    store.dispatch(
      setUserLangAction(
        storage.getItem(STORAGE_KEYS.USER_LANGUAGE) || LANGUAGES[0]
      )
    );
  }
);

i18next.on('languageChanged', (lng) => {
  storage.setItem(STORAGE_KEYS.USER_LANGUAGE, lng);
  store.dispatch(setUserLangAction(lng));
});

export default {};
