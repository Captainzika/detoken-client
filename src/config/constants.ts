export const connections = {
  api: process.env.API_ORIGIN,
};

export const WS_URL = process.env.WS_ORIGIN;

export enum CURRENCY_SYMBOL {
  USD = '$',
  EUR = '€',
  GBP = '£',
}

export const COINBASE_URL = `https://api.coinbase.com/v2/exchange-rates`;

export const CURRENCIES = Object.keys(CURRENCY_SYMBOL);

export const STORAGE_KEYS = {
  USER_CURRENCY: 'userCurrency',
  USER_LANGUAGE: 'userLanguage',
  USER_THEME: 'userTheme',
  USER_EMAIL: 'userEmail',
};

export const LANGUAGES = ['EN', 'RU', '中文'];

export const MIN_WITHDRAWAL = 546;

export const DUST_SATOSHIS = 546;

export const EXPLORER_URL = 'https://explorer.bitcoin.com';

export const CONTRACTS = ['v1-BCH-1d'] as const;

export enum POSITION {
  HEDGE = 'hedge',
  LONG = 'long',
}

export const BLOCKS_IN_DAY = 144;
export const MS_IN_BLOCK = 10 * 60 * 1000;
export const SATOSHIS_IN_BCH = 1e8;
export const CENTS_IN_USD = 100;
export const BCH_DP = 8;

// Taken from anyHedge library
export const MAX_HEDGE_UNITS = 1000000;
export const MIN_HEDGE_UNITS = 50;

export const AH_FEE_DIVISOR = 4;

export const DETOKEN_LINK = 'https://detoken.net';
