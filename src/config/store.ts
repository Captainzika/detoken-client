import persistState from 'redux-localstorage';
import { createStore, compose, applyMiddleware, Dispatch } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import thunk from 'redux-thunk';
import { rootReducer, rootEpic } from '~dux/rootDux';

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
  }
}

interface ReduxAction {
  type: string;
  payload: any;
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const devLog = () => (next: Dispatch) => (action: ReduxAction) => {
  const msgs = [];

  switch (action.type) {
    case 'DEBUG':
      msgs.push('DEBUG:', action.payload);
      break;

    default:
      msgs.push('will dispatch', action);
  }

  console.log(...msgs);
  return next(action);
};

const errorHandler = () => (next: Dispatch) => (action: ReduxAction) => {
  if (action.type === 'ERROR') {
    console.error(action.payload);
    return;
  }
  return next(action);
};

const emptyMiddleware = () => (next: Dispatch) => (action: ReduxAction) =>
  next(action);

const epicMiddleware = createEpicMiddleware();

const middleware = [
  thunk,
  epicMiddleware,
  process.env.NODE_ENV === 'development' ? devLog : emptyMiddleware,
  errorHandler,
];

const enhancer = composeEnhancers(
  applyMiddleware(...middleware),
  process.env.NODE_ENV === 'development'
    ? persistState()
    : applyMiddleware(emptyMiddleware)
);

const store = createStore(rootReducer, enhancer);

epicMiddleware.run(rootEpic);

export default store;
