import { css, SerializedStyles } from '@emotion/core';
import interWoff from '../fonts/Inter/InterRegular.woff';
import interWoff2 from '../fonts/Inter/InterRegular.woff2';
import interTtf from '../fonts/Inter/InterRegular.ttf';

import interBoldWoff from '../fonts/Inter/InterBold.woff';
import interBoldWoff2 from '../fonts/Inter/InterBold.woff2';
import interBoldTtf from '../fonts/Inter/InterBold.ttf';

import { Theme } from './theme';

export const makeGlobalStyles = (theme: Theme): SerializedStyles => css`
  @font-face {
    font-family: 'Inter';
    font-style: normal;
    font-weight: normal;
    src: local('Inter'), local('Inter-Regular'),
      url(${interWoff2}) format('woff2'), url(${interWoff}) format('woff'),
      url(${interTtf}) format('truetype'); /* Safari, Android, iOS */
  }

  @font-face {
    font-family: 'Inter';
    font-style: normal;
    font-weight: bold;
    src: local('Inter'), url(${interBoldWoff2}) format('woff2'),
      url(${interBoldWoff}) format('woff'),
      url(${interBoldTtf}) format('truetype'); /* Safari, Android, iOS */
  }

  html,
  body {
    font-size: 16px;
    margin: 0;
    padding: 0;
    line-height: 1.2;
    outline: none;
    overscroll-behavior: none;
    font-family: 'Inter', serif;
  }

  @media (min-width: 1280px) {
    body,
    html {
      font-size: 18px;
    }
  }

  html {
    min-height: 100%;
    display: flex;
  }

  body {
    flex: 1;
  }

  // fix chrome autocomplete colors
  input:-webkit-autofill,
  input:-webkit-autofill:hover,
  input:-webkit-autofill:focus,
  input:-webkit-autofill:active {
    transition: background-color 5000s ease-in-out 0s;
  }

  #root {
    height: 100%;
  }

  a {
    color: ${theme.text.primary};
    text-decoration: none;
    &:hover {
      text-decoration: underline;
    }
  }
`;
