export interface IContractMetadata {
  oraclePublicKey: string;
  hedgePublicKey: string;
  longPublicKey: string;
  startBlockHeight: number;
  maturityModifier: number;
  earliestLiquidationModifier: number;
  highLiquidationPriceMultiplier: number;
  lowLiquidationPriceMultiplier: number;
  startPrice: number;
  hedgeUnits: number;
  longInputUnits: number;
  totalInputSats: number;
  hedgeInputSats: number;
  longInputSats: number;
  dustCost: number;
  minerCost?: number;
}

export interface IContractParameters {
  lowLiquidationPrice: number;
  highLiquidationPrice: number;
  earliestLiquidationHeight: number;
  maturityHeight: number;
  oraclePubk: string;
  hedgeLockScript: string;
  longLockScript: string;
  hedgeMutualRedeemPubk: string;
  longMutualRedeemPubk: string;
  lowTruncatedZeroes: string;
  highLowDeltaTruncatedZeroes: string;
  hedgeUnitsXSatsPerBchHighTrunc: number;
  payoutSatsLowTrunc: number;
}

export interface ISimulationOutput {
  hedgePayoutSats: number;
  longPayoutSats: number;
  payoutSats: number;
  minerFeeSats: number;
}

enum SettlementType {
  MATURATION = 'maturation',
  LIQUIDATION = 'liquidation',
  MUTUAL = 'mutual',
  EXTERNAL = 'external',
}

interface ContractFunding {
  fundingTransaction: string;
  fundingOutput: number;
  fundingSatoshis: number;
  feeOutput?: number;
  feeSatoshis?: number;
}

interface ContractSettlement {
  spendingTransaction: string;
  settlementType: SettlementType;
  hedgeSatoshis: number;
  longSatoshis: number;
  oracleMessage?: string;
  oraclePublicKey?: string;
  oracleSignature?: string;
  oraclePrice?: number;
}

type ParsedFunding = Omit<
  ContractFunding,
  'fundingSatoshis' | 'feeOutput' | 'feeSatoshis'
>;
type ParsedSettlement = Pick<
  ContractSettlement,
  'spendingTransaction' | 'settlementType'
> &
  Partial<ContractSettlement>;
type ParsedPayoutParameters = Omit<
  IContractParameters,
  'hedgeMutualRedeemPubk' | 'longMutualRedeemPubk'
>;
type ParsedMutualRedemptionParameters = Omit<
  IContractParameters,
  'oraclePubk' | 'hedgeLockScript' | 'longLockScript'
>;

interface ParsedMutualRedemptionData {
  address: string;
  funding: ParsedFunding;
  settlement: ParsedSettlement;
  parameters: ParsedMutualRedemptionParameters;
}

interface ParsedPayoutData {
  address: string;
  funding: ParsedFunding;
  settlement: ParsedSettlement;
  parameters: ParsedPayoutParameters;
}

export type IParsedSettlementData =
  | ParsedMutualRedemptionData
  | ParsedPayoutData;
