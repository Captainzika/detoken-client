module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'jsx-a11y', 'react', 'react-hooks', 'import'],
  extends: [
    'airbnb',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:react/recommended',
    'prettier',
    'prettier/@typescript-eslint',
    'prettier/react',
    'plugin:import/typescript',
    // 'plugin:prettier/recommended'
  ],
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
    jsx: true,
  },
  env: {
    browser: true,
    es6: true,
  },
  rules: {
    // common
    'no-use-before-define': [
      'error',
      {
        functions: false,
        classes: false,
        variables: false,
      },
    ],
    'no-unused-vars': ['error', { varsIgnorePattern: '^_' }],
    'no-underscore-dangle': 2,
    'no-multi-spaces': ['error', { exceptions: { ImportDeclaration: true } }],
    'no-plusplus': 0,
    'no-bitwise': ['error', { allow: ['~'] }],
    'no-unused-expressions': [
      'error',
      { allowShortCircuit: true, allowTernary: true },
    ],
    indent: 'off',
    'no-param-reassign': ['error', { props: false }],
    'no-trailing-spaces': 'off',
    'lines-between-class-members': 'off',
    'import/no-unresolved': [0, { commonjs: true }],
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    'import/prefer-default-export': 0,
    'import/extensions': [
      'error',
      {
        ts: 'never',
        tsx: 'never',
        svg: 'always',
      },
    ],
    'no-console': 1,
    'consistent-return': 0,

    // react-a11y
    'jsx-a11y/click-events-have-key-events': 0,
    'jsx-a11y/no-static-element-interactions': 0,

    // react
    'react/jsx-filename-extension': [1, { extensions: ['.ts', '.tsx'] }],
    'react/jsx-indent-props': 'off',
    'react/jsx-curly-spacing': 'off',
    'react/sort-comp': [
      'error',
      {
        order: [
          'type-annotations',
          '/^defaultProps$/',
          'instance-variables',
          'static-methods',
          'state',
          'lifecycle',
          'render',
          '/^render.+$/',
          'everything-else',
          '/^_.+$/',
        ],
      },
    ],
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
    'react/require-default-props': 'off',
    'react/jsx-indent': 'off',
    'react/jsx-wrap-multilines': 'off',
    'react/no-danger': 'off',
    'react/display-name': 'off',
    // "react/prefer-stateless-function": "off",
    // "react/jsx-closing-bracket-location": "off",
    // "react/react-in-jsx-scope": "off",

    // typescript
    '@typescript-eslint/no-explicit-any': 0,
    '@typescript-eslint/interface-name-prefix': 0,
    '@typescript-eslint/ban-ts-ignore': 1,

    // others
    'prettier/prettier': 'off',
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
  overrides: [
    {
      files: ['*.ts'],
      rules: {
        'no-console': 0,
      },
    },
  ],
};
